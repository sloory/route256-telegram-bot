CURDIR=$(shell pwd)
BINDIR=${CURDIR}/bin
GOVER=$(shell go version | perl -nle '/(go\d\S+)/; print $$1;')
MOCKGEN=${BINDIR}/mockgen_${GOVER}
SMARTIMPORTS=${BINDIR}/smartimports_${GOVER}
LINTVER=v1.49.0
LINTBIN=${BINDIR}/lint_${GOVER}_${LINTVER}
GOOSEBIN=${BINDIR}/goose_${GOVER}
GDBBIN=${BINDIR}/gdg
WAITFOR=${BINDIR}/wait-for
BOT_PACKAGE=gitlab.ozon.dev/sloory.d/telegram-bot/cmd/bot
KAFKA_PACKAGE=gitlab.ozon.dev/sloory.d/telegram-bot/cmd/kafka
BUFBIN=${BINDIR}/buf
export POSTGRES_PASSWORD=pass
export POSTGRES_DB=expense

ifeq ($(POSTGRES_HOST),)
export POSTGRES_HOST
POSTGRES_HOST=localhost
endif

DB_CONNECT_STR="host=${POSTGRES_HOST} user=postgres password=${POSTGRES_PASSWORD} dbname=${POSTGRES_DB} sslmode=disable"

all: format build test lint

build: bindir
	go build -o ${BINDIR}/bot ${BOT_PACKAGE}
	go build -o ${BINDIR}/kafka-report-consumer ${KAFKA_PACKAGE}

test: docker-up migrate
	go clean -testcache
	$(MAKE) install-wait-for
	go test -v -p 1 ./internal/...

test-gitlab: migrate migrate-status
	CI=true go test -v -p 1 ./...

run-consumer:
	go run ${KAFKA_PACKAGE} 2>&1 | tee data/logs/consumer_log.txt

run-dev: docker-up migrate
	go run ${BOT_PACKAGE} -dev

run-prod: docker-up migrate
	go run ${BOT_PACKAGE} 2>&1 | tee data/logs/log.txt

generate: install-mockgen
	${MOCKGEN} \
		-source=internal/model/messages/incoming_msg.go \
		-destination=internal/mocks/messages/messages_mocks.go

lint: install-lint
	${LINTBIN} run ./internal/...

buf-generate: install-buf
	(cd ./internal/infrastructure/grpc/	&& \
		PATH=${PATH}:${BINDIR} && \
		${BUFBIN} generate --path ./proto/api)

precommit: format build test lint
	echo "OK"

bindir:
	mkdir -p ${BINDIR}

format: install-smartimports
	${SMARTIMPORTS} -exclude internal/mocks

install-mockgen: bindir
	test -f ${MOCKGEN} || \
		(GOBIN=${BINDIR} go install github.com/golang/mock/mockgen@v1.6.0 && \
		mv ${BINDIR}/mockgen ${MOCKGEN})

install-lint: bindir
	test -f ${LINTBIN} || \
		(GOBIN=${BINDIR} go install github.com/golangci/golangci-lint/cmd/golangci-lint@${LINTVER} && \
		mv ${BINDIR}/golangci-lint ${LINTBIN})

install-smartimports: bindir
	test -f ${SMARTIMPORTS} || \
		(GOBIN=${BINDIR} go install github.com/pav5000/smartimports/cmd/smartimports@latest && \
		mv ${BINDIR}/smartimports ${SMARTIMPORTS})

install-goose: bindir
	test -f ${GOOSEBIN} || \
		(GOBIN=${BINDIR} go install github.com/pressly/goose/v3/cmd/goose@latest && \
		mv ${BINDIR}/goose ${GOOSEBIN})

install-buf: bindir
	test -f ${BUFBIN} || \
		(GOBIN=${BINDIR} go install github.com/bufbuild/buf/cmd/buf@v1.9.0)

	test -f ${BINDIR}/protoc-gen-go-grpc || \
		(GOBIN=${BINDIR} go install google.golang.org/grpc/cmd/protoc-gen-go-grpc)

	test -f ${BINDIR}/protoc-gen-go || \
		(GOBIN=${BINDIR} go install google.golang.org/protobuf/cmd/protoc-gen-go)

	test -f ${BINDIR}/protoc-gen-grpc-gateway || \
		(GOBIN=${BINDIR} go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway)

	test -f ${BINDIR}/protoc-gen-openapiv2 || \
		(GOBIN=${BINDIR} go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2)

	test -f ${BINDIR}//protoc-gen-validate || \
		(go get -d github.com/envoyproxy/protoc-gen-validate && \
		git clone https://github.com/bufbuild/protoc-gen-validate && \
		cd protoc-gen-validate && GOBIN=${BINDIR} make build && \
		cd ../ && rm -r -f protoc-gen-validate)

docker-up:
	docker-compose -p expense -f docker-compose.yml up -d --remove-orphans
	docker-compose -p expense -f docker-compose.yml logs

docker-down:
	docker-compose -p expense -f docker-compose.yml down

migrate: install-goose
	${GOOSEBIN} -dir ./migrations/ -v postgres ${DB_CONNECT_STR} up

migrate-status: install-goose
	${GOOSEBIN} -dir ./migrations/ -v postgres ${DB_CONNECT_STR} status

wait-for-postgres: install-wait-for
	${WAITFOR} ${POSTGRES_HOST}:5432

install-wait-for:
	test -f ${WAITFOR} || \
		wget -O ${WAITFOR} https://raw.githubusercontent.com/eficode/wait-for/v2.2.3/wait-for && \
		chmod u+x ${WAITFOR}

metrics-up:
	mkdir -p data/grafana
	sudo chmod -R 777 data/grafana
	cd metrics && sudo docker-compose up -d

metrics-down:
	cd metrics && sudo docker-compose down

logs-up:
	mkdir -p data/logs
	touch data/logs/log.txt
	touch data/logs/offsets.yaml
	sudo chmod -R 777 data/logs
	cd logs && docker-compose up -d

logs-down:
	cd logs && sudo docker-compose down

tracing-up:
	cd tracing && sudo docker-compose up -d

tracing-down:
	cd tracing && sudo docker-compose down

metrics-init: install-gdb
	cd metrics && ${GDBBIN} ds export
	cd metrics && ${GDBBIN} dash export

install-gdb: bindir
	test -f ${GDBBIN} || \
		GOBIN=${BINDIR} go install github.com/esnet/gdg@latest

