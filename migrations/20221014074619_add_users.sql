-- +goose Up
-- +goose StatementBegin
create table users (
   id  integer PRIMARY KEY,
   currency_code char(3) not null
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table users;
-- +goose StatementEnd
