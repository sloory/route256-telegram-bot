-- +goose Up
-- +goose StatementBegin
alter table users add month_limit integer null;
alter table users alter column currency_code drop not null;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
alter table users drop column month_limit;
alter table users alter column currency_code set not null;
-- +goose StatementEnd
