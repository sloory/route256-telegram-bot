-- +goose Up
-- +goose StatementBegin
create table rates (
    code char(3) not null,
    rate float not null,

    constraint rates_unique_code_cst unique (code)
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table rates;
-- +goose StatementEnd
