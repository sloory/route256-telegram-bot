package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/clients/tg"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/config"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	apiServer "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/grpc/server"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka/producer"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/logger"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/tracing"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/bot"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

var configPath string
var devMode bool

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)
	defer cancel()

	initFlag()

	log := logger.InitLogger(devMode)
	tracing.InitTracing(log)

	configService, err := config.New(configPath)
	if err != nil {
		log.Fatal("configService init failed", zap.Error(err))
	}

	tgClient, err := tg.New(configService)
	if err != nil {
		log.Fatal("tg client init failed", zap.Error(err))
	}

	var botObj *bot.Bot
	storage := configService.Storage()
	if storage == "in_memory" {
		botObj = bot.NewInMemory(
			tgClient,
			configService.RatesUpdatePeriodMin(),
			configService.ReportCacheSize(),
			log,
		)
	} else {
		db, err := sql.Open("postgres", storage)
		if err != nil {
			log.Fatal("postgres open error", zap.Error(err))
		}

		if err := db.Ping(); err != nil {
			log.Fatal("postgres ping error", zap.Error(err))
		}

		producerConfig := producer.GetProducerConfig()
		producerConfig.Producer.Interceptors =
			append(producerConfig.Producer.Interceptors, producer.NewLogInterceptor(log))

		syncProducer, err := sarama.NewSyncProducer(
			configService.KafkaBrokers(), producerConfig,
		)
		if err != nil {
			log.Fatal("create kafka producer error", zap.Error(err))
		}

		defer func() {
			err := syncProducer.Close()
			if err != nil {
				log.Error("close kafka provider error", zap.Error(err))
			}
		}()

		botObj = bot.NewDB(
			tgClient,
			configService.RatesUpdatePeriodMin(),
			configService.ReportCacheSize(),
			database.New(db, log),
			log,
			expense.NewKafkaReporter(
				producer.NewCreateReportMsgSender(syncProducer, log),
			),
		)

		kafka.RunMetrics(ctx, producerConfig.MetricRegistry, log)
	}

	metrics.Run(ctx, configService.MetricsPort(), log)
	apiServer.RunGRPC(ctx, configService.GRPCApiPort(), tgClient, log)
	apiServer.RunHttp(ctx, configService.HttpApiPort(), configService.GRPCApiPort(), log)
	botObj.Run(ctx, configService.WorkersCount())
}

func initFlag() {
	flag.StringVar(&configPath, "config", "", "config path")
	flag.BoolVar(&devMode, "dev", false, "development mode")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `bot
	start telegram bot
Usage:
	bot [flags]
`)
		flag.PrintDefaults()
	}

	flag.Parse()
}
