package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/config"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	apiClient "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/grpc/client"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka/consumer"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/logger"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/tracing"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

var configPath string
var devMode bool

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)
	defer cancel()

	initFlag()

	log := logger.InitLogger(devMode)
	tracing.InitTracing(log)

	consumerConfig, err := config.LoadReportConsumerConfig(configPath)
	if err != nil {
		log.Fatal("config load failed", zap.Error(err))
	}

	db, err := sql.Open("postgres", consumerConfig.Storage)
	if err != nil {
		log.Fatal("postgres open error", zap.Error(err))
	}

	if err := db.Ping(); err != nil {
		log.Fatal("postgres ping error", zap.Error(err))
	}

	kafkaConfig := consumer.NewConsumerConfig()
	kafkaConfig.Consumer.Interceptors = append(
		kafkaConfig.Consumer.Interceptors,
		consumer.NewLogInterceptor(log),
	)

	kafka.RunMetrics(ctx, kafkaConfig.MetricRegistry, log)

	kafkaGroup := consumer.NewCreateReportConsumerGroup(
		consumerConfig.KafkaBrokers,
		kafkaConfig,
		log,
	)

	apiClientObj := apiClient.NewReportServiceClient(0)
	err = apiClientObj.Connect(ctx)
	if err != nil {
		log.Fatal("api client connect error", zap.Error(err))
	}
	defer func() {
		err := apiClientObj.Close()
		if err != nil {
			log.Fatal("api client close connect error", zap.Error(err))
		}
	}()

	err = kafkaGroup.Start(
		ctx,
		consumer.NewCreateReportConsumer(
			expenseDatabase.NewDBRepository(database.New(db, log)),
			apiClientObj,
		),
	)

	if err != nil {
		log.Fatal("consume error", zap.Error(err))
	}
}

func initFlag() {
	flag.StringVar(&configPath, "config", "", "config path")
	flag.BoolVar(&devMode, "dev", false, "development mode")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `bot
	start telegram bot
Usage:
	bot [flags]
`)
		flag.PrintDefaults()
	}

	flag.Parse()
}
