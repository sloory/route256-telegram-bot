## Start

`make run-prod` - запускает бота с логами в production режиме
`make run-dev` - запускает бота с логами в development режиме


## Logs

`make logs-up` - поднять контейнеры для сбора логов
`make logs-down` - гасит контейнеры для сбора логов

Graylog: http://127.0.0.1:7555/ (admin/admin)

## Metrics

`make metrics-up` - поднять контейнеры для метрик.

`make metrics-init` - инициализировать Grafana:
 - автоматически будет добавдяется Prometheus datasource
 - создасться разработанный dashboard

`make metrics-down` - гасит контейнеры для метрик

Prometheus: http://127.0.0.1:9090/
Grafana: http://127.0.0.1:3000/ (admin/admin)

## Tracing

`make tracing-up` - поднять контейнеры для сбора трейсов
`make tracing-down` - гасит контейнеры для сбора трейсов


Jaeger: http://127.0.0.1:16686/
=======

ДЗ-4. System-design высоконагруженного бота финансовых трат: https://miro.com/app/board/uXjVPLAJmJA=/
