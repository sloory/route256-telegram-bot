package bot_test

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"math"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	ratesService "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/services"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/bot"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_Rates(t *testing.T) {
	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}
	server := startHttpMockServer(t)

	ratesStorage := ratesDatabase.NewInMemoryRepository()
	updater :=
		bot.NewRatesUpdater(
			ratesStorage,
			&ratesService.RatesService{Url: "http://localhost:8181"},
			1,
			tests.Log,
		)

	ctx, cancel := context.WithCancel(context.Background())
	updater.Run(ctx)

	rate := waitAndGetRateFromStorage(t, ctx, ratesStorage)
	assert.Equal(t, 61.2475, math.Round(rate*10000)/10000)

	_ = server.Shutdown(ctx)
	cancel()
}

func Test_BotLogs(t *testing.T) {

	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}
	server := startHttpMockServer(t)

	ratesStorage := ratesDatabase.NewInMemoryRepository()
	log, observedLogs := tests.NewObservedLogger()
	updater :=
		bot.NewRatesUpdater(
			ratesStorage,
			&ratesService.RatesService{Url: "http://localhost:8181"},
			1,
			log,
		)

	ctx, cancel := context.WithCancel(context.Background())
	updater.Run(ctx)

	rate := waitAndGetRateFromStorage(t, ctx, ratesStorage)
	assert.Equal(t, 61.2475, math.Round(rate*10000)/10000)

	_ = server.Shutdown(ctx)
	cancel()

	assert.Equal(t, 3, observedLogs.Len())

	logs := observedLogs.All()

	assert.Equal(t, "Start updated rates", logs[0].Message)
	assert.Equal(t, "Rates for currency not received", logs[1].Message)
	assert.ElementsMatch(t, []zap.Field{
		{Key: "currency", String: "CNY", Type: zapcore.StringType},
	}, logs[1].Context)
	assert.Equal(t, "Rates updated", logs[2].Message)
}

func waitAndGetRateFromStorage(
	t *testing.T,
	ctx context.Context,
	ratesStorage *ratesDatabase.InMemoryRepository,
) float64 {
	var rate float64
	var err error
	i := 0
	for {
		rate, err = ratesStorage.Get(ctx, money.USD)
		if err == nil {
			break
		}
		if err != ratesDatabase.ErrNotHaveRate {
			t.Fatal(err)
			break
		}
		time.Sleep(time.Microsecond * 400)
		i++
		if i > 100 {
			break
		}
	}

	return rate
}

func startHttpMockServer(t *testing.T) *http.Server {
	server := &http.Server{Addr: ":8181", Handler: &MockRateServer{}}
	go func() {
		err := server.ListenAndServe()
		if err != http.ErrServerClosed {
			t.Error(err)
			return
		}
	}()

	return server
}

type MockRateServer struct {
}

func (ctr *MockRateServer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	fmt.Fprint(w,
		`
{
    "disclaimer": "https://www.cbr-xml-daily.ru/#terms",
    "date": "2022-10-08",
    "timestamp": 1665176400,
    "base": "RUB",
    "rates": {
        "USD": 0.016327197,
        "EUR": 0.016673447,
        "KRW": 23.06055,
        "JPY": 2.36777178
    }
}`,
	)
}
