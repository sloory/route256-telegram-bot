package bot

import (
	"context"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	"go.uber.org/zap"
	"time"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type UpdateRubbleRate interface {
	Set(ctx context.Context, currency money.Currency, rate float64) error
}
type RatesService interface {
	Get(context.Context) (map[string]float64, error)
}

const (
	DefaultUpdatePeriodInMin = 10
)

type RatesUpdater struct {
	ratesStorage         UpdateRubbleRate
	ratesService         RatesService
	currencyForUpdate    []money.Currency
	ratesUpdatePeriodMin int
	log                  *zap.Logger
}

func NewRatesUpdater(
	ratesStorage UpdateRubbleRate,
	ratesService RatesService,
	ratesUpdatePeriodMin int,
	log *zap.Logger,
) *RatesUpdater {
	if ratesUpdatePeriodMin == 0 {
		ratesUpdatePeriodMin = DefaultUpdatePeriodInMin
	}

	return &RatesUpdater{
		ratesStorage,
		ratesService,
		[]money.Currency{money.USD, money.EUR, money.CNY},
		ratesUpdatePeriodMin,
		log,
	}
}

func (u *RatesUpdater) Run(ctx context.Context) {
	err := u.ratesStorage.Set(ctx, money.RUB, 1)
	if err != nil {
		u.log.Error("Save rate error", zap.Error(err))
	}
	go func() {
		ticker := time.NewTicker(time.Minute * time.Duration(u.ratesUpdatePeriodMin))
		defer ticker.Stop()
		u.updateRates(ctx)
		for {
			select {
			case <-ticker.C:
				u.updateRates(ctx)
			case <-ctx.Done():
				u.log.Info("Rates updater stopped")
				return
			}
		}
	}()
}

func (u *RatesUpdater) updateRates(ctx context.Context) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "update rates")
	defer span.Finish()

	startTime := time.Now()

	u.log.Info("Start updated rates")
	rates, err := u.ratesService.Get(ctx)
	if err != nil {
		ext.Error.Set(span, true)
		metrics.RateUpdatesErrors.Inc()
		u.log.Error("Get currency rates error", zap.Error(err))
		return
	}

	setCurrencySpan, ctx := opentracing.StartSpanFromContext(ctx, "save rates")
	for _, currency := range u.currencyForUpdate {
		rate, ok := rates[currency.Code]
		if !ok {
			u.log.Info("Rates for currency not received", zap.String("currency", currency.Code))
			metrics.RateUpdatesErrors.Inc()
			continue
		}

		metrics.SetCurrencyRate(currency.Code, rate)

		err := u.ratesStorage.Set(ctx, currency, rate)
		if err != nil {
			metrics.RateUpdatesErrors.Inc()
			u.log.Error("Save rate error", zap.Error(err))
		}
	}
	setCurrencySpan.Finish()

	duration := time.Since(startTime)
	metrics.RateUpdates.Inc()
	metrics.SummaryRateUpdateTime.Observe(duration.Seconds())

	u.log.Info("Rates updated")
}
