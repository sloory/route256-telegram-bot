package bot

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
	"go.uber.org/zap"
	"sync"
)

type WorkerPool struct {
	ctx       context.Context
	count     int
	log       *zap.Logger
	processor *messages.Processor
}

func NewWorkerPool(
	ctx context.Context,
	count int,
	log *zap.Logger,
	processor *messages.Processor,
) *WorkerPool {
	return &WorkerPool{ctx, count, log, processor}
}

func (p *WorkerPool) Create(in <-chan messages.Message) chan messages.Message {
	out := make(chan messages.Message, sendQueueSize)
	wg := sync.WaitGroup{}
	wg.Add(p.count)
	for i := 0; i < p.count; i++ {
		go msgProcessorWorker(p.log, p.ctx, &wg, in, out, p.processor)
	}

	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func msgProcessorWorker(
	log *zap.Logger,
	ctx context.Context,
	wg *sync.WaitGroup,
	in <-chan messages.Message,
	out chan<- messages.Message,
	processor *messages.Processor,
) {
	defer wg.Done()
stopWork:
	for {
		select {
		case <-ctx.Done():
			break stopWork
		case msg := <-in:
			response, err := processor.Process(ctx, msg)
			if err != nil {
				log.Error("error processing message:", zap.Error(err))
				continue
			}
			if response != nil {
				out <- messages.Message{
					Text:   response.String(),
					UserID: msg.UserID,
				}
			} else {
				out <- messages.Message{}
			}
		}
	}
}
