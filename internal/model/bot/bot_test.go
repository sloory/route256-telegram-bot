package bot

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
)

func Test_ProcessMessageByWorkers(t *testing.T) {
	in := make(chan messages.Message)
	bot := NewDB(
		NewMockTgClient(), 0, 1, tests.GetTestDB(), tests.Log, nil,
	)
	out := bot.createWorkerPool(context.Background(), 1).Create(in)

	in <- messages.Message{
		Text:   "/start",
		UserID: 123,
	}
	close(in)
	msg := <-out

	assert.Equal(t, "hello", msg.Text)
	assert.Equal(t, 123, int(msg.UserID))
}

func Test_DBIntegrationForGetExpenseReport(t *testing.T) {
	defer tests.ClearDB()

	in := make(chan messages.Message)
	bot := NewDB(
		NewMockTgClient(), 0, 1, tests.GetTestDB(), tests.Log, nil,
	)
	out := bot.createWorkerPool(context.Background(), 1).Create(in)

	in <- messages.Message{
		Text:   "/addExpense 100 Еда",
		UserID: 123,
	}
	<-out
	in <- messages.Message{
		Text:   "/addExpense 200 Вода",
		UserID: 123,
	}
	<-out
	in <- messages.Message{
		Text:   "/addExpense 50 Еда",
		UserID: 123,
	}
	<-out
	in <- messages.Message{
		Text:   "/report",
		UserID: 123,
	}
	close(in)

	msg := <-out

	assert.Equal(
		t,
		`Report for all time:
Вода - 200.00 ₽
Еда - 150.00 ₽
`,
		msg.Text)
}

type MockTgClient struct {
	name   string
	userID int64
	ch     chan messages.Message
}

func NewMockTgClient() *MockTgClient {
	return &MockTgClient{ch: make(chan messages.Message)}
}

func (m *MockTgClient) SendMessage(name string, userID int64) error {
	m.name = name
	m.userID = userID
	return nil
}

func (m *MockTgClient) GetIncomingChannel() <-chan messages.Message {
	return m.ch
}

func (m *MockTgClient) StopReceivingUpdates() {
	// nothing to do
}
