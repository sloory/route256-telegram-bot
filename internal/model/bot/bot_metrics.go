package bot

import (
	"context"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
	"time"
)

type MetricsPipeline struct {
	inner PipelineBuilder
}

func (t *MetricsPipeline) Create(in <-chan messages.Message) chan messages.Message {
	out := make(chan messages.Message)
	innerIn := make(chan messages.Message)
	innerOut := t.inner.Create(innerIn)
	go func() {
		defer func() {
			close(out)
			close(innerIn)
		}()

		for msg := range in {
			metrics.Requests.Inc()
			metrics.InFlightRequests.Inc()

			startTime := time.Now()
			innerIn <- msg
			msg = <-innerOut

			duration := time.Since(startTime)
			metrics.HistogramResponseTime.Observe(duration.Seconds())
			metrics.InFlightRequests.Dec()

			out <- msg
		}
	}()
	return out
}

type MetricsByCommand struct {
	inner messages.CommandHandler
}

func (h *MetricsByCommand) Handle(ctx context.Context, userID expense.UserId, commandName string, text string) (messages.CommandResponse, error) {
	startTime := time.Now()
	response, err := h.inner.Handle(ctx, userID, commandName, text)
	duration := time.Since(startTime)

	if err == nil {
		metrics.CommandsCount.With(prometheus.Labels{"command": commandName}).Inc()
		metrics.HistogramCommandResponseTime.With(prometheus.Labels{"command": commandName}).Observe(duration.Seconds())
	}

	return response, err
}
