package bot

import (
	"context"
	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
)

type TracingCommand struct {
	inner messages.CommandHandler
}

func (h *TracingCommand) Handle(ctx context.Context, userID expense.UserId, commandName string, text string) (messages.CommandResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "process command")
	span.LogKV("command", commandName)
	defer span.Finish()

	return h.inner.Handle(ctx, userID, commandName, text)
}
