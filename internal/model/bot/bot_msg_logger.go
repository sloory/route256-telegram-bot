package bot

import (
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
	"go.uber.org/zap"
)

type IncomingMessageLogger struct {
	log *zap.Logger
}

func (l *IncomingMessageLogger) Create(in <-chan messages.Message) chan messages.Message {
	return addInPipeline(
		func(msg messages.Message) messages.Message {
			l.log.Info(
				"incoming message",
				zap.Int64("userId", int64(msg.UserID)),
				zap.String("message", msg.Text),
			)

			return msg
		},
		in,
	)
}
