package bot

import (
	"context"
	_ "github.com/lib/pq"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/cache"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	userDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/user"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	ratesService "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/services"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
	"go.uber.org/zap"
)

const (
	defaultWorkersCount = 5
	defaultCacheSize    = 100
	sendQueueSize       = 10
)

type MessageSender interface {
	SendMessage(text string, userID int64) error
	GetIncomingChannel() <-chan messages.Message
	StopReceivingUpdates()
}

type RubbleRater interface {
	messages.GetRubbleRate
	UpdateRubbleRate
}

type Transactor interface {
	WithinTransaction(context.Context, func(ctx context.Context) error) error
}

type Locker interface {
	Lock(ctx context.Context, id expense.UserId) func()
}

type PipelineBuilder interface {
	Create(in <-chan messages.Message) chan messages.Message
}

type Cache interface {
	Set(key string, value any)
	Get(key string) any
	Remove(key string) bool
	Len() int
}

type Storages struct {
	rates      RubbleRater
	expenses   messages.ExpenseStorage
	reporter   messages.ExpenseReporter
	users      messages.UserDataStorage
	transactor Transactor
	locker     Locker
	cache      Cache
}

type Bot struct {
	tgClient     MessageSender
	processor    *messages.Processor
	ratesUpdater *RatesUpdater
	log          *zap.Logger
}

func NewInMemory(
	tgClient MessageSender,
	ratesUpdatePeriodMin int,
	cacheSize int,
	log *zap.Logger,
) *Bot {
	expenseStorage := expenseDatabase.NewThreadSafeExpenseStorage(expenseDatabase.NewInMemoryRepository())
	return New(
		tgClient,
		ratesUpdatePeriodMin,
		Storages{
			rates:      ratesDatabase.NewThreadSafeRatesStorage(ratesDatabase.NewInMemoryRepository()),
			expenses:   expenseStorage,
			reporter:   expenseStorage,
			users:      userDatabase.NewThreadSafeUserDataStorage(userDatabase.NewInMemoryRepository()),
			transactor: &database.FakeTransactor{},
			locker:     &expenseDatabase.MutexExpenseLocker{},
			cache: metrics.NewCacheWithMetrics(
				cache.NewLRUCache(cacheSize),
			),
		},
		log,
	)
}

func NewDB(
	tgClient MessageSender,
	ratesUpdatePeriodMin int,
	cacheSize int,
	db *database.DB,
	log *zap.Logger,
	reporter messages.ExpenseReporter,
) *Bot {
	expenseStorage := expenseDatabase.NewDBRepository(db)
	if reporter == nil {
		reporter = expenseStorage
	}
	return New(
		tgClient,
		ratesUpdatePeriodMin,
		Storages{
			rates:      ratesDatabase.NewDBRepository(db),
			expenses:   expenseStorage,
			reporter:   reporter,
			users:      userDatabase.NewDBRepository(db),
			transactor: db,
			locker:     expenseDatabase.NewDBExpenseLocker(db, log),
			cache: metrics.NewCacheWithMetrics(
				cache.NewLRUCache(cacheSize),
			),
		},
		log,
	)
}

func New(
	tgClient MessageSender,
	ratesUpdatePeriodMin int,
	storage Storages,
	log *zap.Logger,
) *Bot {
	commands := messages.NewCommands(
		storage.rates,
		storage.expenses,
		storage.reporter,
		storage.users,
		storage.transactor,
		storage.locker,
		storage.cache,
	)
	processor := messages.NewProcessor()
	processor.AddHandler(
		&MetricsByCommand{&TracingCommand{commands}},
	)

	model := Bot{
		tgClient:  tgClient,
		processor: processor,
		ratesUpdater: NewRatesUpdater(
			storage.rates,
			ratesService.NewRatesService(log),
			ratesUpdatePeriodMin,
			log,
		),
		log: log,
	}

	return &model
}

func (b *Bot) Run(ctx context.Context, workersCount int) {
	if workersCount == 0 {
		workersCount = defaultWorkersCount
	}
	b.log.Info("Bot start listening for messages")

	b.ratesUpdater.Run(ctx)

	out :=
		(&MetricsPipeline{b.createWorkerPool(ctx, workersCount)}).
			Create(
				(&IncomingMessageLogger{b.log}).
					Create(b.tgClient.GetIncomingChannel()),
			)

stopWork:
	for {
		select {
		case <-ctx.Done():
			b.log.Error("stop receiving TG updates")
			b.tgClient.StopReceivingUpdates()
			break stopWork
		case msg := <-out:
			if !msg.Empty() {
				err := b.tgClient.SendMessage(msg.Text, int64(msg.UserID))
				if err != nil {
					b.log.Error("error send message:", zap.Error(err))
				}
			}
		}
	}

	b.log.Info("Bot stopped")
}

func (b *Bot) createWorkerPool(ctx context.Context, count int) *WorkerPool {
	return NewWorkerPool(ctx, count, b.log, b.processor)
}

func addInPipeline(
	tFunc func(in messages.Message) messages.Message,
	in <-chan messages.Message,
) chan messages.Message {
	out := make(chan messages.Message)
	go func() {
		defer close(out)
		for msg := range in {
			out <- tFunc(msg)
		}
	}()
	return out
}
