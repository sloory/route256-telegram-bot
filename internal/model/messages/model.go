package messages

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
	"time"
)

type Message struct {
	Text   string
	UserID expense.UserId
}

func (m *Message) Empty() bool {
	return m.UserID == 0 && m.Text == ""
}

type ExpenseReporter interface {
	ReportStartFrom(context.Context, expense.UserId, time.Time) (expense.Report, error)
}

type ExpenseStorage interface {
	Add(context.Context, expense.UserId, *expense.Expense) error
	Clear(context.Context, expense.UserId) error
	ReportStartFrom(context.Context, expense.UserId, time.Time) (expense.Report, error)
	ExpenseStartFrom(context.Context, expense.UserId, time.Time) (money.Money, error)
}

type UserCurrencyStorage interface {
	SetCurrency(context.Context, expense.UserId, money.Currency) error
	GetCurrency(context.Context, expense.UserId) (money.Currency, error)
}

type UserDataStorage interface {
	UserCurrencyStorage
	UserLimitsStorage
}

type GetRubbleRate interface {
	Get(ctx context.Context, currency money.Currency) (float64, error)
}

type CommandResponse interface {
	String() string
}

type Transactor interface {
	WithinTransaction(context.Context, func(ctx context.Context) error) error
}

type Locker interface {
	Lock(ctx context.Context, id expense.UserId) func()
}

type Cache interface {
	Set(key string, value any)
	Get(key string) any
	Remove(key string) bool
	Len() int
}

type ErrorResponse struct {
	Error string
}

func (r ErrorResponse) String() string {
	return "Error: " + r.Error
}

type TextResponse struct {
	Text string
}

func (r TextResponse) String() string {
	return r.Text
}

type CommandHelp struct {
	Description string
	Example     string
}

type CommandHandler interface {
	Handle(ctx context.Context, userID expense.UserId, commandName string, text string) (CommandResponse, error)
}

type Command interface {
	CommandHandler
	help() CommandHelp
}
