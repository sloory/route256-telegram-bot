package messages

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	userLimitsDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/user"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
)

func Test_GetLimitWithoutLimit(t *testing.T) {
	command := NewLimitCommand(
		userLimitsDatabase.NewInMemoryRepository(),
	)

	response, err := command.Handle(
		context.Background(),
		expense.UserId(1),
		"limit",
		"",
	)

	assert.NoError(t, err)
	assert.Equal(t, "You not have limits", response.String())
}

func Test_SetAndGetLimit(t *testing.T) {
	command := NewLimitCommand(
		userLimitsDatabase.NewInMemoryRepository(),
	)

	response, err := command.Handle(
		context.Background(),
		expense.UserId(1),
		"limit",
		"100.01",
	)

	assert.NoError(t, err)
	assert.Equal(t, "Monthly limit set", response.String())

	response, err = command.Handle(
		context.Background(),
		expense.UserId(1),
		"limit",
		"",
	)

	assert.NoError(t, err)
	assert.Equal(t, "Your monthly limit is 100.01 ₽", response.String())
}
