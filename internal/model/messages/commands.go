package messages

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type Commands struct {
	list map[string]Command
}

func NewCommands(
	rates GetRubbleRate,
	expenseStorage ExpenseStorage,
	expenseReporter ExpenseReporter,
	userStorage UserDataStorage,
	transactor Transactor,
	locker Locker,
	cache Cache,
) *Commands {
	commands := &Commands{make(map[string]Command)}

	reporterCached := expense.NewReporterCached(
		cache, expenseStorage, expenseReporter,
	)

	exchanger := &money.Exchanger{RubbleRateService: rates}
	commands.Add("start", &StartCommand{})
	commands.Add("help", &HelpCommand{commands: commands})
	commands.Add("addExpense", NewAddExpenseCommand(
		expense.NewAddExpenseWithConversion(
			exchanger,
			expense.NewAddExpenseWithLimit(
				locker,
				userStorage,
				expenseStorage,
				expense.NewAddRubbleExpenseToStorage(reporterCached),
				transactor,
			),
		),
		userStorage,
	))

	commands.Add("report", NewReportCommand(
		expense.NewReporterWithConversion(
			exchanger,
			reporterCached,
			money.RUB,
		),
		userStorage,
	))
	commands.Add("clear", NewClearCommand(expenseStorage))
	commands.Add("currency", NewCurrencyCommand(userStorage))
	commands.Add("limit", NewLimitCommand(userStorage))

	return commands
}

func (c *Commands) Add(name string, command Command) {
	c.list[name] = command
}

func (c *Commands) Handle(ctx context.Context, userID expense.UserId, commandName string, text string) (CommandResponse, error) {
	command, ok := c.list[commandName]
	if !ok {
		return &ErrorResponse{"unknown command name"}, nil
	}

	return command.Handle(ctx, userID, commandName, text)
}
