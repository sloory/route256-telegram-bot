package messages

import (
	"context"
	"regexp"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type AddExpenseToStorage interface {
	Add(context.Context, expense.UserId, *expense.Expense) error
}

type AddExpenseCommand struct {
	storage             AddExpenseToStorage
	userCurrencyStorage UserCurrencyStorage
}

func NewAddExpenseCommand(storage AddExpenseToStorage, userCurrency UserCurrencyStorage) *AddExpenseCommand {
	return &AddExpenseCommand{storage: storage, userCurrencyStorage: userCurrency}
}
func (c *AddExpenseCommand) Handle(ctx context.Context, userId expense.UserId, commandName string, text string) (CommandResponse, error) {

	currency, err := c.userCurrencyStorage.GetCurrency(ctx, userId)
	if err != nil {
		return nil, errors.Wrap(err, "while add expense")
	}

	expenseObj, err := parseExpense(text, currency)
	if err != nil {
		if errors.Is(err, strconv.ErrRange) {
			return &ErrorResponse{"To big amount value"}, nil
		}
		return &ErrorResponse{err.Error()}, nil
	}

	err = c.storage.Add(ctx, userId, expenseObj)
	if err != nil {
		return &ErrorResponse{err.Error()}, nil
	}

	return &TextResponse{"Your expense added"}, nil
}

var expenseCommandRe = regexp.MustCompile(`^\s*(\S+)\s+(.+?)\s*(\s+(\d+\.\d+\.\d+)\s*)?$`)

var errIncorrectAddExpenseCommand = errors.New("wrong add expense command format")
var errWrongDateFormat = errors.New("wrong date format")

func parseExpense(text string, currency money.Currency) (*expense.Expense, error) {
	matches := expenseCommandRe.FindStringSubmatch(text)

	if len(matches) != 5 {
		return nil, errIncorrectAddExpenseCommand
	}

	amount, err := money.Parse(matches[1], currency)
	if err != nil {
		return nil, err
	}
	category := matches[2]
	dateStr := matches[4]

	// case without date
	if dateStr == "" {
		expenseObj, err := expense.NewTodayExpense(amount, category)
		if err != nil {
			return nil, err
		}
		return expenseObj, nil
	}

	date, err := time.Parse("02.01.2006", matches[4])
	if err != nil {
		return nil, errWrongDateFormat
	}

	expenseObj, err := expense.NewExpense(amount, date, category)
	if err != nil {
		return nil, err
	}

	return expenseObj, nil
}

func (c *AddExpenseCommand) help() CommandHelp {
	return CommandHelp{
		Description: "add information about expense",
		Example: "/addExpense <amount> <category> <date>(optional)\n" +
			"/addExpense 100.11 Ипотека  - for today expense \n" +
			"/addExpense 100.11 За кредит 30.01.2022\n",
	}
}
