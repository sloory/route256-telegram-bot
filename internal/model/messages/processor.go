package messages

import (
	"context"
	"github.com/pkg/errors"
	"regexp"
)

type Processor struct {
	handlers []CommandHandler
}

func NewProcessor() *Processor {
	processor := Processor{}
	processor.handlers = make([]CommandHandler, 0, 2)
	return &processor
}

func (s *Processor) AddHandler(handler CommandHandler) {
	s.handlers = append(s.handlers, handler)
}

func (s *Processor) Process(ctx context.Context, msg Message) (CommandResponse, error) {
	commandName, other, err := parseCommand(msg.Text)
	if err != nil {
		return &ErrorResponse{"command not found"}, nil
	}

	var response CommandResponse
	response = nil
	for _, handler := range s.handlers {
		response, err = handler.Handle(ctx, msg.UserID, commandName, other)
		if err != nil {
			return nil, err
		}
	}
	return response, nil
}

var errCommandNotFound = errors.New("command not found")
var commandRe = regexp.MustCompile(`^\/(\w+)\s*(.*?)\s*$`)

func parseCommand(msg string) (string, string, error) {
	matches := commandRe.FindStringSubmatch(msg)

	if len(matches) < 3 {
		return "", "", errCommandNotFound
	}

	commandName := matches[1]
	other := matches[2]

	return commandName, other, nil
}
