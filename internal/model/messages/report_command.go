package messages

import (
	"context"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type ReportResponse struct {
	Report      expense.Report
	PeriodTitle string
}

func (r ReportResponse) String() string {
	response := "Report for " + r.PeriodTitle + ":\n"

	if len(r.Report) == 0 {
		response += "No expenses for period\n"
	} else {
		categories := make([]string, 0, len(r.Report))
		for k := range r.Report {
			categories = append(categories, k)
		}
		sort.Strings(categories)
		for _, category := range categories {
			amount := r.Report[category]
			response += fmt.Sprintf("%s - %s\n", category, amount)
		}
	}
	return response
}

type ReporterWithConversion interface {
	expense.Reporter
	ToCurrency(currency money.Currency) expense.Reporter
}

type ReportCommand struct {
	reporter            ReporterWithConversion
	userCurrencyStorage UserCurrencyStorage
}

func NewReportCommand(reporter ReporterWithConversion, storage UserCurrencyStorage) *ReportCommand {
	return &ReportCommand{reporter: reporter, userCurrencyStorage: storage}
}
func (c *ReportCommand) Handle(ctx context.Context, userId expense.UserId, commandName string, text string) (CommandResponse, error) {
	reportPeriod, err := parseReportPeriod(text)
	if err != nil {
		return nil, err
	}

	currency, err := c.userCurrencyStorage.GetCurrency(ctx, userId)
	if err != nil {
		return nil, errors.Wrap(err, "while make Report")
	}
	reporter := c.reporter.ToCurrency(currency)

	var report expense.Report
	var startDate time.Time
	if reportPeriod.IsEmpty() {
		startDate = time.Time{}
	} else {
		startDate = reportPeriod.StartFrom(time.Now())
	}

	report, err = reporter.ReportStartFrom(ctx, userId, startDate)

	if err != nil {
		if err == expense.ErrCanNotConvertToCurrency {
			return &ErrorResponse{"Can not convert Report to your currency, sorry"}, nil
		}
		return nil, err
	}

	if report == nil {
		return nil, nil
	}

	return &ReportResponse{Report: report, PeriodTitle: reportPeriod.String()}, nil
}

var periodRe = regexp.MustCompile(`^\s*((\d*)y)?\s*((\d*)m)?\s*((\d*)d)?\s*$`)

var errWrongReportPeriod = errors.New("wrong Report period")

func parseReportPeriod(text string) (expense.ReportPeriod, error) {
	matches := periodRe.FindStringSubmatch(text)

	if len(matches) != 7 {
		return expense.ReportPeriod{}, errWrongReportPeriod
	}

	getValue := func(text string, valueExist bool) (int, error) {
		if !valueExist {
			return 0, nil
		}
		if text == "" {
			return 1, nil
		}
		return strconv.Atoi(text)
	}

	years, err := getValue(matches[2], matches[1] != "")
	if err != nil {
		return expense.ReportPeriod{}, err
	}
	months, err := getValue(matches[4], matches[3] != "")
	if err != nil {
		return expense.ReportPeriod{}, err
	}
	days, err := getValue(matches[6], matches[5] != "")
	if err != nil {
		return expense.ReportPeriod{}, err
	}

	return expense.ReportPeriod{
		Years:  years,
		Months: months,
		Days:   days,
	}, nil
}

func (c *ReportCommand) help() CommandHelp {
	return CommandHelp{
		Description: "expense Report by categories",
		Example: "/report w - for last week report\n" +
			"/report m - for last month report\n" +
			"/report y - for last year report\n" +
			"/report 2w 3m 1y - for last year 3 month and 2 weeks report\n",
	}
}
