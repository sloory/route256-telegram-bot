package messages

import (
	"context"
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type UserLimitsStorage interface {
	SetLimit(context.Context, expense.UserId, money.Money) error
	GetLimit(context.Context, expense.UserId) (money.Money, error)
}

type LimitCommand struct {
	storage UserLimitsStorage
}

func NewLimitCommand(storage UserLimitsStorage) *LimitCommand {
	return &LimitCommand{storage: storage}
}
func (c *LimitCommand) Handle(ctx context.Context, userId expense.UserId, commandName string, text string) (CommandResponse, error) {

	if text == "" {
		limit, err := c.storage.GetLimit(ctx, userId)
		if err != nil {
			return nil, errors.Wrap(err, "while get user limit")
		}

		if limit.Value == 0 {
			return &TextResponse{"You not have limits"}, nil
		}

		return &TextResponse{fmt.Sprintf("Your monthly limit is %s", limit.String())}, nil
	}

	limit, err := money.Parse(strings.Trim(text, " "), money.RUB)
	if err != nil {
		return &ErrorResponse{err.Error()}, nil
	}

	err = c.storage.SetLimit(ctx, userId, limit)
	if err != nil {
		return nil, errors.Wrap(err, "while ser user limit")
	}
	return &TextResponse{"Monthly limit set"}, nil
}

func (c *LimitCommand) help() CommandHelp {
	return CommandHelp{
		Description: "set/get user limits",
		Example: "/limit  - show your current monthly limit\n" +
			"/limit 101.01  - set 100.01 Rubble as your monthly limit\n",
	}
}
