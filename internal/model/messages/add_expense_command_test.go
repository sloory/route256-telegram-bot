package messages

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_ParseWithoutDate(t *testing.T) {
	expenseObj, err := parseExpense("100 Еда", money.RUB)
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(100, 0), expenseObj.Amount)
	assert.Equal(t, "Еда", expenseObj.Category)
}

func Test_ParseWithExtraSpaces(t *testing.T) {
	expenseObj, err := parseExpense("    101   Еда   12.01.2022  ", money.RUB)
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(101, 0), expenseObj.Amount)
	assert.Equal(t, "Еда", expenseObj.Category)
	assert.Equal(
		t,
		time.Date(2022, 01, 12, 0, 0, 0, 0, time.UTC),
		expenseObj.Date,
	)
}

func Test_ParseCategoryWithSpace(t *testing.T) {
	expenseObj, err := parseExpense("100   За лагерь детям   ", money.RUB)
	assert.NoError(t, err)
	assert.Equal(t, "За лагерь детям", expenseObj.Category)
}

func Test_ParseAmountWithKopek(t *testing.T) {
	expenseObj, err := parseExpense("100.01 Еда", money.RUB)
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(100, 1), expenseObj.Amount)
}

func Test_ParseWrongFormat(t *testing.T) {
	_, err := parseExpense("Еда за 100", money.RUB)
	assert.Error(t, err)
}

func Test_ParseAmountOutOfRangeError(t *testing.T) {
	_, err := parseExpense("10011111111111111111111111 Еда", money.RUB)
	assert.Error(t, err)
}

func Test_ParseAmountWithToLongKopeks(t *testing.T) {
	_, err := parseExpense("100.133333333333301 Еда", money.RUB)
	assert.Error(t, err)
}

func Test_ParseWithDate(t *testing.T) {
	expenseObj, err := parseExpense("100 Еда 12.01.2022", money.RUB)
	assert.NoError(t, err)
	assert.Equal(
		t,
		time.Date(2022, 01, 12, 0, 0, 0, 0, time.UTC),
		expenseObj.Date,
	)
}

func Test_ParseWithWrongDate(t *testing.T) {
	_, err := parseExpense("100 Еда 12.01.20222", money.RUB)
	assert.Error(t, err)
}
