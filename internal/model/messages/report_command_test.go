package messages

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	userCurrencyDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/user"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_EmptyReportResponseToString(t *testing.T) {
	report := make(expense.Report)

	assert.Equal(
		t,
		`Report for week:
No expenses for period
`,
		(&ReportResponse{Report: report, PeriodTitle: "week"}).String(),
	)
}

func Test_ReportResponseToString(t *testing.T) {
	report := make(expense.Report)
	report["За еду"] = money.NewRub(1, 0)
	report["Ипотека"] = money.NewRub(1, 0)

	assert.Equal(
		t,
		`Report for week:
За еду - 1.00 ₽
Ипотека - 1.00 ₽
`,
		(&ReportResponse{Report: report, PeriodTitle: "week"}).String(),
	)
}

func Test_ParseSimplyReportPeriod(t *testing.T) {
	now := time.Now()

	reportType, err := parseReportPeriod("d")
	assert.NoError(t, err)
	assert.Equal(
		t,
		now.AddDate(0, 0, -1),
		reportType.StartFrom(now),
	)
	assert.Equal(t, "day", reportType.String())

	reportType, err = parseReportPeriod("m")
	assert.NoError(t, err)
	assert.Equal(
		t,
		now.AddDate(0, -1, 0),
		reportType.StartFrom(now),
	)
	assert.Equal(t, "month", reportType.String())

	reportType, err = parseReportPeriod("y")
	assert.NoError(t, err)
	assert.Equal(
		t,
		now.AddDate(-1, 0, 0),
		reportType.StartFrom(now),
	)
	assert.Equal(t, "year", reportType.String())
}

func Test_ParseReportPeriodWithNumbers(t *testing.T) {
	now := time.Now()

	reportType, err := parseReportPeriod("3y 2m 1d")
	assert.NoError(t, err)
	assert.Equal(
		t,
		now.AddDate(-3, -2, -1),
		reportType.StartFrom(now),
	)
	assert.Equal(t, "3 years 2 months day", reportType.String())
}

func Test_ReportWeekCommand(t *testing.T) {

	repo := expenseDatabase.NewInMemoryRepository()
	expenseObj, _ := expense.NewExpense(
		money.NewRub(1, 0),
		time.Now().AddDate(0, 0, -1),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	// out of week expense
	expenseObj, _ = expense.NewExpense(
		money.NewRub(1, 0),
		time.Now().AddDate(0, 0, -8),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewTodayExpense(money.NewRub(2, 0), "Газ")
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	command := createReportCommand(repo)
	response, err := command.Handle(context.Background(), expense.UserId(1), "Report", "7d")

	assert.NoError(t, err)
	assert.Equal(
		t,
		`Report for 7 days:
Газ - 2.00 ₽
Еда - 1.00 ₽
`,
		response.String(),
	)
}

func Test_ReportForAllTime(t *testing.T) {

	repo := expenseDatabase.NewInMemoryRepository()
	expenseObj, _ := expense.NewExpense(
		money.NewRub(1, 0),
		time.Now().AddDate(0, 0, -1),
		"Еда",
	)
	_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)

	expenseObj, _ = expense.NewExpense(
		money.NewRub(1, 0),
		time.Now().AddDate(-100, 0, -8),
		"Еда",
	)
	_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)

	command := createReportCommand(repo)
	response, err := command.Handle(context.Background(), expense.UserId(1), "Report", "")

	assert.NoError(t, err)
	assert.Equal(
		t,
		`Report for all time:
Еда - 2.00 ₽
`,
		response.String(),
	)
}

func createReportCommand(repo *expenseDatabase.InMemoryRepository) *ReportCommand {
	return NewReportCommand(
		expense.NewReporterWithConversion(
			&money.Exchanger{RubbleRateService: ratesDatabase.NewInMemoryRepository()},
			repo,
			money.RUB,
		),
		userCurrencyDatabase.NewInMemoryRepository(),
	)
}
