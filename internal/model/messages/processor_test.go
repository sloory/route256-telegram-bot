package messages

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/cache"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	userDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/user"
)

func Test_ParseCommand_WithCorrectFormatReturnCommandNameAndOther(t *testing.T) {
	command, other, err := parseCommand("/start текст c пробелом")
	assert.NoError(t, err)

	assert.Equal(t, "start", command)
	assert.Equal(t, "текст c пробелом", other)
}

func Test_ParseCommand_WithJustCommand(t *testing.T) {
	command, other, err := parseCommand("/help")
	assert.NoError(t, err)

	assert.Equal(t, "help", command)
	assert.Empty(t, "", other)
}

func Test_ParseCommand_ErrorWithoutCommandInMessage(t *testing.T) {
	_, _, err := parseCommand("Привет бот!")
	assert.Error(t, err)
}

func Test_ParseCommand_ErrorWithDoubleSlash(t *testing.T) {
	_, _, err := parseCommand("//start")
	assert.Error(t, err)
}

func Test_OnStartCommand_ShouldAnswerWithIntroMessage(t *testing.T) {
	response, err := createProcessor().Process(
		context.Background(),
		Message{
			Text:   "/start",
			UserID: 123,
		})

	assert.NoError(t, err)
	assert.IsType(t, &TextResponse{}, response)
	assert.Equal(t, "hello", response.String())
}

func Test_OnStartCommand_ShouldAnswerWithHelpMessage(t *testing.T) {
	response, err := createProcessor().Process(
		context.Background(),
		Message{
			Text:   "some text",
			UserID: 123,
		})

	assert.NoError(t, err)
	assert.IsType(t, &ErrorResponse{}, response)
	assert.Equal(t, "command not found", response.(*ErrorResponse).Error)
}

func createProcessor() *Processor {
	expenseRepo := expenseDatabase.NewInMemoryRepository()
	commands := NewCommands(
		ratesDatabase.NewInMemoryRepository(),
		expenseRepo,
		expenseRepo,
		userDatabase.NewInMemoryRepository(),
		&database.FakeTransactor{},
		&expenseDatabase.MutexExpenseLocker{},
		&cache.NoCache{},
	)
	processor := NewProcessor()
	processor.AddHandler(commands)

	return processor
}
