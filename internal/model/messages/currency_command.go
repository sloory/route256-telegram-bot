package messages

import (
	"context"
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type CurrencyCommand struct {
	storage UserCurrencyStorage
}

func NewCurrencyCommand(storage UserCurrencyStorage) *CurrencyCommand {
	return &CurrencyCommand{storage: storage}
}
func (c *CurrencyCommand) Handle(ctx context.Context, userId expense.UserId, commandName string, text string) (CommandResponse, error) {

	if text == "" {
		currency, err := c.storage.GetCurrency(ctx, userId)
		if err != nil {
			return nil, errors.Wrap(err, "while change user currency")
		}
		return &TextResponse{fmt.Sprintf("Your currency is %s", currency.String())}, nil
	}

	currency, err := money.CurrencyByCode(strings.Trim(text, " "))
	if err != nil {
		return &ErrorResponse{err.Error()}, nil
	}

	err = c.storage.SetCurrency(ctx, userId, currency)
	if err != nil {
		return nil, errors.Wrap(err, "while change user currency")
	}
	return &TextResponse{"Your currency changed"}, nil
}

func (c *CurrencyCommand) help() CommandHelp {
	return CommandHelp{
		Description: "set/get currenct currency",
		Example: "/currency  - show your current currency\n" +
			"/currency USD - set USD as your current currency\n",
	}
}
