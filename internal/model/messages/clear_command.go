package messages

import (
	"context"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
)

type ClearStorage interface {
	Clear(context.Context, expense.UserId) error
}

type ClearCommand struct {
	storage ClearStorage
}

func NewClearCommand(storage ClearStorage) *ClearCommand {
	return &ClearCommand{storage: storage}
}
func (c *ClearCommand) Handle(ctx context.Context, userId expense.UserId, commandName string, text string) (CommandResponse, error) {
	err := c.storage.Clear(ctx, userId)
	if err != nil {
		return nil, err
	}
	return &TextResponse{"Your expenses deleted"}, nil
}

func (c *ClearCommand) help() CommandHelp {
	return CommandHelp{
		Description: "delete information about your expenses",
	}
}
