package messages

import (
	"context"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
)

type StartCommand struct{}

func (c *StartCommand) Handle(ctx context.Context, userID expense.UserId, commandName string, text string) (CommandResponse, error) {
	return &TextResponse{"hello"}, nil
}
func (c *StartCommand) help() CommandHelp {
	return CommandHelp{
		Description: "show welcome message",
	}
}

type HelpCommand struct {
	commands *Commands
}

func (c *HelpCommand) Handle(ctx context.Context, userID expense.UserId, commandName string, text string) (CommandResponse, error) {
	response := ""
	for name := range c.commands.list {
		help := (c.commands.list)[name].help()

		response += "- /" + name + " " + help.Description + "\n"
		if help.Example != "" {
			response += "\t\t Examples:\n"
			response += "\t\t" + help.Example + "\n"
		}
	}
	return &TextResponse{response}, nil
}
func (c *HelpCommand) help() CommandHelp {
	return CommandHelp{
		Description: "show bot commands",
	}
}
