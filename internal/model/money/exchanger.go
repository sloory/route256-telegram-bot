package money

import (
	"context"

	"github.com/pkg/errors"
)

var ErrCrossExchangeRateNowAllowed = errors.New("Cross currency exchange rates not allowed")

type Exchanger struct {
	RubbleRateService GetRubbleRate
}

func (c *Exchanger) Convert(ctx context.Context, from Money, currency Currency) (Money, error) {
	if from.Currency != RUB && currency != RUB {
		return Money{}, ErrCrossExchangeRateNowAllowed
	}

	var otherCurrency Currency
	if from.Currency == RUB {
		otherCurrency = currency
	} else {
		otherCurrency = from.Currency
	}

	if from.Currency == currency {
		return from, nil
	}

	rate, err := c.RubbleRateService.Get(ctx, otherCurrency)
	if err != nil {
		return Money{}, errors.Wrap(err, "Convert from")
	}

	var result Money
	if from.Currency == RUB {
		result = from.Mul(1 / rate)
	} else {
		result = from.Mul(rate)
	}

	result.Currency = currency
	return result, nil
}

type GetRubbleRate interface {
	Get(ctx context.Context, currency Currency) (float64, error)
}
