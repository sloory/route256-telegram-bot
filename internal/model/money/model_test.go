package money_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_CurrencyByCode(t *testing.T) {
	currency, err := money.CurrencyByCode("USD")
	assert.NoError(t, err)
	assert.Equal(t, money.USD, currency)
	currency, err = money.CurrencyByCode("RUB")
	assert.NoError(t, err)
	assert.Equal(t, money.RUB, currency)
	currency, err = money.CurrencyByCode("EUR")
	assert.NoError(t, err)
	assert.Equal(t, money.EUR, currency)
	currency, err = money.CurrencyByCode("CNY")
	assert.NoError(t, err)
	assert.Equal(t, money.CNY, currency)
}

func Test_Test_CurrencyByWrongCode(t *testing.T) {
	_, err := money.CurrencyByCode("YEN")
	assert.ErrorIs(t, err, money.ErrWrongCurrency)
}

func Test_AmountMul(t *testing.T) {
	assert.Equal(
		t,
		money.New(150, 0, money.RUB),
		money.New(100, 0, money.RUB).Mul(1.5),
	)
	assert.Equal(
		t,
		money.New(150, 1, money.RUB),
		money.New(100, 1, money.RUB).Mul(1.5),
	)
	assert.Equal(
		t,
		money.New(190, 1, money.RUB),
		money.New(100, 1, money.RUB).Mul(1.9),
	)
}

func Test_ToString(t *testing.T) {
	assert.Equal(t, "150.01 ₽", money.New(150, 1, money.RUB).String())
	assert.Equal(t, "150.10 ₽", money.New(150, 10, money.RUB).String())
	assert.Equal(t, "150.10 $", money.New(150, 10, money.USD).String())
	assert.Equal(t, "150.10 €", money.New(150, 10, money.EUR).String())
	assert.Equal(t, "150.10 ¥", money.New(150, 10, money.CNY).String())
}

func Test_ParseOnlyWithMajorUnit(t *testing.T) {
	amount, err := money.Parse("100", money.RUB)
	assert.NoError(t, err)
	assert.Equal(
		t,
		money.NewRub(100, 0),
		amount,
	)
}

func Test_ParseOnlyWithMajorAndMinorUnit(t *testing.T) {
	amount, err := money.Parse("100.01", money.USD)
	assert.NoError(t, err)
	assert.Equal(
		t,
		money.NewUsd(100, 1),
		amount,
	)
}

func Test_ParseError(t *testing.T) {
	_, err := money.Parse("сто рублей", money.USD)
	assert.Error(t, err)
}
