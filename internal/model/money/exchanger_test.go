package money_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_ConvertFromRubbleToRubble(t *testing.T) {

	exchanger := money.Exchanger{RubbleRateService: ratesDatabase.NewInMemoryRepository()}
	from := money.New(100, 10, money.RUB)
	to, err := exchanger.Convert(context.Background(), from, money.RUB)
	assert.NoError(t, err)

	assert.Equal(t, from, to)
}

func Test_ConvertFromRubble(t *testing.T) {
	repo := ratesDatabase.NewInMemoryRepository()
	err := repo.Set(context.Background(), money.USD, 70)
	assert.NoError(t, err)

	exchanger := money.Exchanger{RubbleRateService: repo}

	from := money.New(7007, 0, money.RUB)
	to, err := exchanger.Convert(context.Background(), from, money.USD)
	assert.NoError(t, err)

	assert.Equal(
		t,
		money.New(100, 10, money.USD),
		to,
	)
}

func Test_ConvertToRubble(t *testing.T) {
	repo := ratesDatabase.NewInMemoryRepository()
	err := repo.Set(context.Background(), money.USD, 70)
	assert.NoError(t, err)

	exchanger := money.Exchanger{RubbleRateService: repo}

	from := money.New(100, 10, money.USD)
	to, err := exchanger.Convert(context.Background(), from, money.RUB)
	assert.NoError(t, err)

	assert.Equal(
		t,
		money.New(7007, 0, money.RUB),
		to,
	)
}

func Test_ErrorForConvertNotRubbleToNotRubble(t *testing.T) {
	exchanger := money.Exchanger{RubbleRateService: ratesDatabase.NewInMemoryRepository()}
	from := money.New(100, 10, money.USD)
	_, err := exchanger.Convert(context.Background(), from, money.CNY)
	assert.ErrorIs(t, err, money.ErrCrossExchangeRateNowAllowed)
}
