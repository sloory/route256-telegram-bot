package money

import (
	"fmt"
	"math"
	"regexp"
	"strconv"

	"github.com/pkg/errors"
)

var ErrWrongCurrency = errors.New("Wrong currency")

type Currency struct {
	Code  string
	short string
}

func CurrencyByCode(code string) (Currency, error) {
	switch code {
	case "USD":
		return USD, nil
	case "RUB":
		return RUB, nil
	case "EUR":
		return EUR, nil
	case "CNY":
		return CNY, nil
	}

	return RUB, ErrWrongCurrency
}

func (c Currency) String() string {
	return c.short
}

var RUB = Currency{"RUB", "₽"}
var CNY = Currency{"CNY", "¥"}
var EUR = Currency{"EUR", "€"}
var USD = Currency{"USD", "$"}

type Money struct {
	Value    int
	Currency Currency
}

var moneyRe = regexp.MustCompile(`^(\d+)(\.(\d{2}))?$`)
var errAmount = errors.New("wrong money value format")

func Parse(str string, currency Currency) (Money, error) {
	matches := moneyRe.FindStringSubmatch(str)

	if len(matches) != 4 {
		return NewRub(0, 0), errAmount
	}

	ruble, err := strconv.Atoi(matches[1])
	if err != nil {
		return NewRub(0, 0), err
	}
	var kopek int
	if matches[3] != "" {
		kopek, err = strconv.Atoi(matches[3])
		if err != nil {
			return NewRub(0, 0), err
		}
	} else {
		kopek = 0
	}

	return New(ruble, kopek, currency), nil
}

func New(major int, minor int, currency Currency) Money {
	return Money{Value: major*100 + minor, Currency: currency}
}

func NewRub(major int, minor int) Money {
	return New(major, minor, RUB)
}

func NewUsd(major int, minor int) Money {
	return New(major, minor, USD)
}

func NewEur(major int, minor int) Money {
	return New(major, minor, EUR)
}

func (m Money) String() string {
	return fmt.Sprintf("%d.%02d %s", m.Value/100, m.Value%100, m.Currency.String())
}

func (m Money) Mul(rate float64) Money {
	return Money{int(math.Floor(float64(m.Value) * rate)), m.Currency}
}
