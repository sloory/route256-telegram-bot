package expense_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_EmptyReportPeriod(t *testing.T) {
	assert.True(t, expense.ReportPeriod{}.IsEmpty())
	assert.Equal(t, "all time", expense.ReportPeriod{}.String())
}

func Test_ReportPeriodWithAtLeastOnePartsIsNotEmpty(t *testing.T) {
	assert.False(t, expense.ReportPeriod{Days: 1}.IsEmpty())
	assert.False(t, expense.ReportPeriod{Months: 1}.IsEmpty())
	assert.False(t, expense.ReportPeriod{Years: 1}.IsEmpty())
}

func Test_ReportStartFromWithConversionToRubble(t *testing.T) {
	reporter, _ := createReporter(money.RUB)
	report, _ := reporter.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.Equal(
		t,
		expense.Report{
			"Еда":     money.NewRub(100, 0),
			"Ипотека": money.NewRub(400, 0),
		},
		report,
	)
}

func Test_ReportStartFromWithConversionToNotRubble(t *testing.T) {
	reporter, rates := createReporter(money.USD)
	err := rates.Set(context.Background(), money.USD, 50)
	assert.NoError(t, err)

	report, _ := reporter.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.Equal(
		t,
		expense.Report{
			"Еда":     money.NewUsd(2, 0),
			"Ипотека": money.NewUsd(8, 0),
		},
		report,
	)
}

func Test_ReportForAllTimeWithConversionToNotRubble(t *testing.T) {
	reporter, rates := createReporter(money.USD)
	err := rates.Set(context.Background(), money.USD, 50)
	assert.NoError(t, err)

	report, _ := reporter.ReportStartFrom(context.Background(), expense.UserId(1), time.Time{})
	assert.Equal(
		t,
		expense.Report{
			"Еда":     money.NewUsd(2, 0),
			"Ипотека": money.NewUsd(8, 0),
		},
		report,
	)
}

func createReporter(currency money.Currency) (*expense.ReporterWithConversion, *ratesDatabase.InMemoryRepository) {
	repo := expenseDatabase.NewInMemoryRepository()
	expenseObj, _ := expense.NewExpense(money.NewRub(100, 0), time.Now(), "Еда")
	_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)

	expenseObj, _ = expense.NewExpense(money.NewRub(400, 0), time.Now(), "Ипотека")
	_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)

	rateService := ratesDatabase.NewInMemoryRepository()
	exchanger := &money.Exchanger{RubbleRateService: rateService}
	return expense.NewReporterWithConversion(exchanger, repo, currency), rateService
}
