package expense

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type UserId int64

type Expense struct {
	Date     time.Time
	Amount   money.Money
	Category string
}

func NewTodayExpense(amount money.Money, category string) (*Expense, error) {
	return NewExpense(amount, time.Now(), category)
}

var errDateInFuture = errors.New("Date can not be in future")
var errDateIsEmpty = errors.New("Date can not be empty")
var errWrongAmount = errors.New("invalid Amount value")
var errEmptyCategory = errors.New("Category can not be empty")

func NewExpense(amount money.Money, date time.Time, category string) (*Expense, error) {
	if date.IsZero() {
		return nil, errDateIsEmpty
	}

	if date.After(time.Now()) {
		return nil, errDateInFuture
	}

	if amount.Value <= 0 {
		return nil, errWrongAmount
	}

	if category == "" {
		return nil, errEmptyCategory
	}

	return &Expense{
		Date:     date,
		Amount:   amount,
		Category: category,
	}, nil
}
