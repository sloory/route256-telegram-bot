package expense

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

var ErrCanStoreNotRubbleExpense = errors.New("can not store not rubble expense")

type AddRubbleExpenseToStorage struct {
	inner AddExpenseToStorage
}

func NewAddRubbleExpenseToStorage(
	inner AddExpenseToStorage,
) *AddRubbleExpenseToStorage {
	return &AddRubbleExpenseToStorage{inner}
}

func (a *AddRubbleExpenseToStorage) Add(ctx context.Context, userId UserId, expense *Expense) error {
	if expense.Amount.Currency != money.RUB {
		return ErrCanStoreNotRubbleExpense
	}

	return a.inner.Add(ctx, userId, expense)
}
