package expense

import (
	"context"
	"fmt"
	"time"
)

type Cache interface {
	Set(key string, value any)
	Get(key string) any
	Remove(key string) bool
}

type ReporterCached struct {
	cached     map[UserId]map[time.Time]struct{}
	cache      Cache
	addExpense AddExpenseToStorage
	reporter   Reporter
}

func NewReporterCached(
	cache Cache,
	addExpense AddExpenseToStorage,
	reporter Reporter,
) *ReporterCached {
	return &ReporterCached{
		cached:     make(map[UserId]map[time.Time]struct{}),
		cache:      cache,
		addExpense: addExpense,
		reporter:   reporter,
	}
}

func (r *ReporterCached) ReportStartFrom(ctx context.Context, userId UserId, startFrom time.Time) (Report, error) {
	key := CacheKey(userId, startFrom)
	cached := r.cache.Get(key)
	if cached != nil {
		return cached.(Report), nil
	}

	report, err := r.reporter.ReportStartFrom(ctx, userId, startFrom)
	if err != nil {
		return nil, err
	}

	if report == nil {
		return nil, nil
	}

	r.cache.Set(key, report)

	if _, exists := r.cached[userId]; !exists {
		r.cached[userId] = make(map[time.Time]struct{})
	}
	var t struct{}
	r.cached[userId][startFrom] = t

	return report, nil
}

func (r *ReporterCached) Add(ctx context.Context, userId UserId, expense *Expense) error {
	if cachedTimes, exists := r.cached[userId]; exists {
		for cachedTime := range cachedTimes {
			if cachedTime.Before(expense.Date) || cachedTime.Equal(expense.Date) {
				r.cache.Remove(CacheKey(userId, cachedTime))
				delete(r.cached[userId], cachedTime)
			}
		}
	}

	return r.addExpense.Add(ctx, userId, expense)
}

func CacheKey(userId UserId, startFrom time.Time) string {
	return fmt.Sprintf("%d_%d_%d_%d", userId, startFrom.Year(), startFrom.Month(), startFrom.Day())
}
