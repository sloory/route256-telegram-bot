package expense

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type AddExpenseToStorage interface {
	Add(context.Context, UserId, *Expense) error
}

type Locker interface {
	Lock(ctx context.Context, id UserId) func()
}

type Transactor interface {
	WithinTransaction(context.Context, func(ctx context.Context) error) error
}

type AddExpenseWithConversion struct {
	exchanger Exchanger
	inner     AddExpenseToStorage
}

type UserLimiter interface {
	GetLimit(context.Context, UserId) (money.Money, error)
}

type UserExpenseGetter interface {
	ExpenseStartFrom(context.Context, UserId, time.Time) (money.Money, error)
}

func NewAddExpenseWithConversion(
	exchanger *money.Exchanger,
	inner AddExpenseToStorage,
) *AddExpenseWithConversion {
	return &AddExpenseWithConversion{exchanger, inner}
}

func (a *AddExpenseWithConversion) Add(ctx context.Context, userId UserId, expense *Expense) error {
	rubAmount, err := a.exchanger.Convert(ctx, expense.Amount, money.RUB)
	if err != nil {
		return err
	}

	rubExpense, err := NewExpense(rubAmount, expense.Date, expense.Category)
	if err != nil {
		return err
	}

	return a.inner.Add(ctx, userId, rubExpense)
}

type AddExpenseWithLimit struct {
	locker     Locker
	limits     UserLimiter
	expenses   UserExpenseGetter
	inner      AddExpenseToStorage
	transactor Transactor
}

func NewAddExpenseWithLimit(
	locker Locker,
	limits UserLimiter,
	expenses UserExpenseGetter,
	inner AddExpenseToStorage,
	transactor Transactor,
) *AddExpenseWithLimit {
	return &AddExpenseWithLimit{
		locker:     locker,
		limits:     limits,
		expenses:   expenses,
		inner:      inner,
		transactor: transactor,
	}
}

var ErrExpenseNotInRubble = errors.New("Expense not in rubble")
var ErrLimitExceeded = errors.New("Limit exceeded")

func (a *AddExpenseWithLimit) Add(ctx context.Context, userId UserId, expense *Expense) error {

	if expense.Amount.Currency != money.RUB {
		return ErrExpenseNotInRubble
	}

	return a.transactor.WithinTransaction(
		ctx,
		func(ctx context.Context) error {
			unlock := a.locker.Lock(ctx, userId)
			defer unlock()

			limit, err := a.limits.GetLimit(ctx, userId)
			if err != nil {
				return errors.Wrap(err, "when add expense")
			}
			if limit.Value == 0 {
				return a.inner.Add(ctx, userId, expense)
			}

			curExpense, err := a.expenses.ExpenseStartFrom(ctx, userId, monthStart(time.Now()))
			if err != nil {
				return errors.Wrap(err, "when add expense")
			}

			if curExpense.Value+expense.Amount.Value > limit.Value {
				return ErrLimitExceeded
			}

			return a.inner.Add(ctx, userId, expense)
		},
	)
}

func monthStart(now time.Time) time.Time {
	year, month, _ := now.Date()
	return time.Date(year, month, 1, 0, 0, 0, 0, now.Location())
}
