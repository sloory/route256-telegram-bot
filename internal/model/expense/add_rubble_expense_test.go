package expense_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_ErrorIfAddExpenseNotInRubbles(t *testing.T) {
	repo := expense.NewAddRubbleExpenseToStorage(
		expenseDatabase.NewInMemoryRepository(),
	)
	expenseObj, _ := expense.NewTodayExpense(
		money.NewUsd(100, 0),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.ErrorIs(t, err, expense.ErrCanStoreNotRubbleExpense)
}

func Test_AddExpenseWithInRubbleWithoutError(t *testing.T) {
	rawRepo := expenseDatabase.NewInMemoryRepository()
	repo := expense.NewAddRubbleExpenseToStorage(rawRepo)

	expenseObj, _ := expense.NewTodayExpense(
		money.NewRub(100, 0),
		"Еда",
	)

	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseStored, _ := rawRepo.ExpenseStartFrom(context.Background(), expense.UserId(1), time.Time{})
	assert.Equal(t, 10000, expenseStored.Value)
}
