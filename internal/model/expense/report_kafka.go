package expense

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka/producer"
	"time"
)

type KafkaSender interface {
	Send(userId int64, startDate time.Time) error
}

type KafkaReporter struct {
	sender KafkaSender
}

func NewKafkaReporter(sender *producer.CreateReportMsgSender) *KafkaReporter {
	return &KafkaReporter{sender: sender}
}

func (r *KafkaReporter) ReportStartFrom(ctx context.Context, userId UserId, startFrom time.Time) (Report, error) {
	err := r.sender.Send(int64(userId), startFrom)
	return nil, err
}
