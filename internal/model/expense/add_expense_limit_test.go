package expense_test

import (
	"context"
	"testing"
	"time"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	userDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/user"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
)

type testCase struct {
	addExpense  *expense.AddExpenseWithLimit
	expenseRepo expenseDatabase.ExpenseStorage
}

func Test_AddExpenseInsideLimit(t *testing.T) {
	userId := expense.UserId(1)
	testCase := createAddExpenseWithLimitInMemory(userId, 20000, 10)

	expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 11), "Еда")
	err := testCase.addExpense.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	sum, _ := testCase.expenseRepo.ExpenseStartFrom(
		context.Background(),
		userId,
		time.Now().Add(-time.Hour),
	)

	assert.Equal(t, money.NewRub(100, 21), sum)
}

func Test_AddExpenseAboveLimit(t *testing.T) {
	userId := expense.UserId(1)
	testCase := createAddExpenseWithLimitInMemory(userId, 20000, 19999)

	expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 11), "Еда")
	err := testCase.addExpense.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.ErrorIs(t, err, expense.ErrLimitExceeded)

	sum, _ := testCase.expenseRepo.ExpenseStartFrom(
		context.Background(),
		userId,
		time.Now().Add(-time.Hour),
	)

	assert.Equal(t, money.NewRub(199, 99), sum)
}

func Test_AddExpenseInParallel(t *testing.T) {
	userId := expense.UserId(1)
	limit := 20001

	for _, scenario := range []testCase{
		createAddExpenseWithLimitInDB(userId, limit, 1),
		createAddExpenseWithLimitInMemory(userId, limit, 1),
	} {
		nInLimit := 10
		nOverLimit := 10
		errs := make(chan error)
		for i := 0; i < nInLimit+nOverLimit; i++ {
			go func() {
				expenseObj, _ := expense.NewTodayExpense(money.NewRub(limit/100/nInLimit, 00), "Еда")
				err := scenario.addExpense.Add(context.Background(), expense.UserId(1), expenseObj)
				errs <- err
			}()
		}

		errorsCount := 0
		for i := 0; i < nInLimit+nOverLimit; i++ {
			err := <-errs
			if err != nil {
				errorsCount++
			}
		}

		assert.Equal(t, nOverLimit, errorsCount)

		sum, _ := scenario.expenseRepo.ExpenseStartFrom(
			context.Background(),
			userId,
			time.Now().Add(-time.Hour),
		)

		assert.Equal(t, money.NewRub(200, 01), sum)
	}
}

func createAddExpenseWithLimitInMemory(userId expense.UserId, limit int, curExpenseValue int) testCase {
	expenseRepo := expenseDatabase.NewInMemoryRepository()

	usersRepo := userDatabase.NewInMemoryRepository()
	_ = usersRepo.SetLimit(
		context.Background(),
		userId,
		money.Money{Value: limit, Currency: money.RUB},
	)

	curExpense, _ := expense.NewTodayExpense(
		money.Money{Value: curExpenseValue, Currency: money.RUB},
		"Food",
	)
	_ = expenseRepo.Add(context.Background(), userId, curExpense)

	return testCase{
		expense.NewAddExpenseWithLimit(
			&expenseDatabase.MutexExpenseLocker{},
			usersRepo,
			expenseRepo,
			expenseRepo,
			&database.FakeTransactor{},
		),
		expenseRepo,
	}
}

func createAddExpenseWithLimitInDB(userId expense.UserId, limit int, curExpenseValue int) testCase {
	db := tests.GetTestDB()
	expenseRepo := expenseDatabase.NewDBRepository(db)

	usersRepo := userDatabase.NewDBRepository(db)
	_ = usersRepo.SetLimit(
		context.Background(),
		userId,
		money.Money{Value: limit, Currency: money.RUB},
	)

	curExpense, _ := expense.NewTodayExpense(
		money.Money{Value: curExpenseValue, Currency: money.RUB},
		"Food",
	)
	_ = expenseRepo.Add(context.Background(), userId, curExpense)

	return testCase{
		expense.NewAddExpenseWithLimit(
			expenseDatabase.NewDBExpenseLocker(db, tests.Log),
			usersRepo,
			expenseRepo,
			expenseRepo,
			db,
		),
		expenseRepo,
	}
}
