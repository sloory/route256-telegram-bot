package expense

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_ErrorIfCreateExpenseWithZeroAmount(t *testing.T) {
	_, err := NewExpense(money.NewRub(0, 0), time.Now(), "Еда")

	assert.ErrorIs(t, errWrongAmount, err)
}

func Test_ErrorCreateExpenseWithDateInFuture(t *testing.T) {
	_, err := NewExpense(money.NewRub(0, 10), time.Now().AddDate(0, 0, 1), "Еда")
	assert.ErrorIs(t, errDateInFuture, err)
}

func Test_ErrorCreateExpenseWithoutDate(t *testing.T) {
	_, err := NewExpense(money.NewRub(0, 10), time.Time{}, "Еда")
	assert.ErrorIs(t, errDateIsEmpty, err)
}

func Test_ErrorCreateExpenseWithEmptyCategory(t *testing.T) {
	_, err := NewExpense(money.NewRub(0, 10), time.Now(), "")
	assert.ErrorIs(t, errEmptyCategory, err)
}
