package expense_test

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/cache"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_ReportStartFrom_withEmptyCache_reportPlacedInCache(t *testing.T) {
	reporter, cache := createReporterCached()

	report, _ := reporter.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))

	assert.Equal(t, 1, cache.Len())
	cacheKey := expense.CacheKey(expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.Equal(t, report, cache.Get(cacheKey))
}

func Test_Add_withFilledCache_invalidateReportsBeforeExpenseDate(t *testing.T) {
	reporter, reportsCache := createReporterCached()

	ctx := context.Background()
	userId := expense.UserId(1)

	yesterdayReport, _ := reporter.ReportStartFrom(ctx, userId, time.Now().AddDate(0, 0, -1))
	_, _ = reporter.ReportStartFrom(ctx, userId, time.Now().AddDate(0, 0, -2))
	_, _ = reporter.ReportStartFrom(ctx, userId, time.Now().AddDate(0, 0, -3))
	_, _ = reporter.ReportStartFrom(ctx, userId, time.Now().AddDate(0, 0, -4))
	assert.Equal(t, 4, reportsCache.Len())

	expenseObj, _ := expense.NewExpense(
		money.NewRub(400, 0),
		time.Now().AddDate(0, 0, -2),
		"Ипотека",
	)
	_ = reporter.Add(ctx, userId, expenseObj)

	assert.Equal(t, 1, reportsCache.Len())
	cacheKey := expense.CacheKey(expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.Equal(t, yesterdayReport, reportsCache.Get(cacheKey))
}

func createReporterCached() (*expense.ReporterCached, *cache.LRUCache) {
	repo := expenseDatabase.NewInMemoryRepository()
	expenseObj, _ := expense.NewExpense(money.NewRub(100, 0), time.Now(), "Еда")
	_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)

	expenseObj, _ = expense.NewExpense(money.NewRub(400, 0), time.Now(), "Ипотека")
	_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)

	cache := cache.NewLRUCache(10)
	return expense.NewReporterCached(
			cache,
			repo,
			repo,
		),
		cache
}
