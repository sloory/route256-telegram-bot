package expense

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

var ErrCanNotConvertToCurrency = errors.New("Cat not convert report to currency")

type Report map[string]money.Money

type ReportPeriod struct {
	Days   int
	Months int
	Years  int
}

func (rt ReportPeriod) String() string {
	if rt.IsEmpty() {
		return "all time"
	}

	titleParts := make([]string, 0, 3)
	if rt.Years == 1 {
		titleParts = append(titleParts, "year")
	} else if rt.Years > 1 {
		titleParts = append(titleParts, fmt.Sprintf("%d years", rt.Years))
	}
	if rt.Months == 1 {
		titleParts = append(titleParts, "month")
	} else if rt.Months > 1 {
		titleParts = append(titleParts, fmt.Sprintf("%d months", rt.Months))
	}
	if rt.Days == 1 {
		titleParts = append(titleParts, "day")
	} else if rt.Days > 1 {
		titleParts = append(titleParts, fmt.Sprintf("%d days", rt.Days))
	}
	return strings.Join(titleParts, " ")
}

func (rt ReportPeriod) IsEmpty() bool {
	return rt.Years == 0 && rt.Months == 0 && rt.Days == 0
}

func (rt ReportPeriod) StartFrom(now time.Time) time.Time {
	return now.AddDate(-rt.Years, -rt.Months, -rt.Days)
}

type Exchanger interface {
	Convert(ctx context.Context, from money.Money, currency money.Currency) (money.Money, error)
}

type Reporter interface {
	ReportStartFrom(context.Context, UserId, time.Time) (Report, error)
}

type ReporterWithConversion struct {
	exchanger  Exchanger
	inner      Reporter
	toCurrency money.Currency
}

func NewReporterWithConversion(
	exchanger Exchanger,
	inner Reporter,
	currency money.Currency,
) *ReporterWithConversion {
	return &ReporterWithConversion{exchanger, inner, currency}
}

func (r *ReporterWithConversion) ReportStartFrom(ctx context.Context, userId UserId, time time.Time) (Report, error) {
	innerReport, err := r.inner.ReportStartFrom(ctx, userId, time)
	if err != nil {
		return nil, err
	}
	return r.convertReport(ctx, innerReport)
}

func (r *ReporterWithConversion) convertReport(ctx context.Context, report Report) (Report, error) {
	if report == nil {
		return nil, nil
	}
	result := make(Report)
	for category := range report {
		amount, err := r.exchanger.Convert(ctx, report[category], r.toCurrency)
		if err != nil {
			return nil, ErrCanNotConvertToCurrency
		}
		result[category] = amount
	}
	return result, nil
}

func (r *ReporterWithConversion) ToCurrency(currency money.Currency) Reporter {
	return NewReporterWithConversion(r.exchanger, r.inner, currency)
}
