package expense_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	expenseDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	ratesDatabase "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/rates"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_AddExpenseWithInRubble(t *testing.T) {
	addExpense, _, expenseRepo := createAddExpenseWithConversion(money.RUB)
	expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 10), "Еда")
	err := addExpense.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, _ := expenseRepo.ReportStartFrom(context.Background(), expense.UserId(1), time.Time{})
	assert.Equal(
		t,
		expense.Report{
			"Еда": money.NewRub(100, 10),
		},
		report,
	)
}

func Test_AddExpenseNotInRubble(t *testing.T) {
	addExpense, ratesRepo, expenseRepo := createAddExpenseWithConversion(money.USD)
	err := ratesRepo.Set(context.Background(), money.USD, 50)
	assert.NoError(t, err)
	err = ratesRepo.Set(context.Background(), money.EUR, 40)
	assert.NoError(t, err)

	expenseObj, _ := expense.NewTodayExpense(money.NewUsd(10, 0), "Еда")
	err = addExpense.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewTodayExpense(money.NewEur(5, 0), "Еда")
	err = addExpense.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, _ := expenseRepo.ReportStartFrom(context.Background(), expense.UserId(1), time.Time{})
	assert.Equal(
		t,
		expense.Report{
			"Еда": money.NewRub(700, 00),
		},
		report,
	)
}

func createAddExpenseWithConversion(currency money.Currency) (
	*expense.AddExpenseWithConversion,
	*ratesDatabase.InMemoryRepository,
	*expenseDatabase.InMemoryRepository,
) {
	repo := expenseDatabase.NewInMemoryRepository()
	rateService := ratesDatabase.NewInMemoryRepository()
	exchanger := &money.Exchanger{RubbleRateService: rateService}
	return expense.NewAddExpenseWithConversion(exchanger, repo), rateService, repo
}
