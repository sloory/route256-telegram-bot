package tests

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
	"log"
)

var Log *zap.Logger

func init() {
	logger, err := zap.Config{
		Level:            zap.NewAtomicLevelAt(zap.DebugLevel),
		Development:      true,
		Encoding:         "console",
		EncoderConfig:    zap.NewDevelopmentEncoderConfig(),
		OutputPaths:      []string{},
		ErrorOutputPaths: []string{},
	}.Build()

	if err != nil {
		log.Fatal("can not create logger for tests")
		return
	}

	Log = logger
}

func NewObservedLogger() (*zap.Logger, *observer.ObservedLogs) {
	observedZapCore, observedLogs := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)
	return observedLogger, observedLogs
}
