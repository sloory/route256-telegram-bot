package tests

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
)

var testDB *sql.DB

func GetTestDB() *database.DB {
	return database.New(getSqlDB(), Log)
}

func ClearDB() {
	db := getSqlDB()
	_, err := db.Exec("delete from expenses;")
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec("delete from categories;")
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec("delete from rates;")
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec("delete from users;")
	if err != nil {
		log.Fatal(err)
	}
}

func getSqlDB() *sql.DB {
	var err error
	if testDB == nil {
		connectStr := fmt.Sprintf(
			"host=%s port=5432 user=%s password=%s dbname=%s sslmode=disable",
			os.Getenv("POSTGRES_HOST"),
			"postgres",
			os.Getenv("POSTGRES_PASSWORD"),
			os.Getenv("POSTGRES_DB"),
		)

		testDB, err = sql.Open("postgres", connectStr)
		if err != nil {
			log.Fatal(err)
		}
	}

	return testDB
}
