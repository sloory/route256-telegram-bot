package config

import (
	"os"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

const defaultConfigFile = "data/config.yaml"

type Config struct {
	Token                string   `yaml:"token"`
	Workers              int      `yaml:"workers"`
	RatesUpdatePeriodMin int      `yaml:"ratesUpdatePeriodMin"`
	Storage              string   `yaml:"storage"`
	MetricsPort          int      `yaml:"metricsPort"`
	ReportCacheSize      int      `yaml:"reportCacheSize"`
	KafkaBrokers         []string `yaml:"kafkaBrokers,flow"`
	HttpPort             int      `yaml:"httpPort"`
	grpcPort             int      `yaml:"grpcPort"`
}

type Service struct {
	config Config
}

func New(path string) (*Service, error) {
	if path == "" {
		path = defaultConfigFile
	}

	rawYAML, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.Wrap(err, "reading config file")
	}

	s := &Service{}
	err = yaml.Unmarshal(rawYAML, &s.config)
	if err != nil {
		return nil, errors.Wrap(err, "parsing yaml")
	}
	return s, nil
}

func (s *Service) Token() string {
	return s.config.Token
}

func (s *Service) WorkersCount() int {
	return s.config.Workers
}

func (s *Service) RatesUpdatePeriodMin() int {
	return s.config.RatesUpdatePeriodMin
}

func (s *Service) Storage() string {
	return s.config.Storage
}

func (s *Service) MetricsPort() int {
	return s.config.MetricsPort
}

func (s *Service) ReportCacheSize() int {
	return s.config.ReportCacheSize
}

func (s *Service) KafkaBrokers() []string {
	return s.config.KafkaBrokers
}

func (s *Service) HttpApiPort() int {
	return s.config.HttpPort
}

func (s *Service) GRPCApiPort() int {
	return s.config.grpcPort
}
