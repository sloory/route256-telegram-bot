package config

import (
	"os"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

const defaultReportConsumerConfigFile = "data/report-consumer-config.yaml"

type ReportConsumerConfig struct {
	Storage      string   `yaml:"storage"`
	KafkaBrokers []string `yaml:"kafkaBrokers,flow"`
}

func LoadReportConsumerConfig(path string) (*ReportConsumerConfig, error) {
	if path == "" {
		path = defaultReportConsumerConfigFile
	}

	rawYAML, err := os.ReadFile(path)
	if err != nil {
		return nil, errors.Wrap(err, "reading config file")
	}

	var config ReportConsumerConfig
	err = yaml.Unmarshal(rawYAML, &config)
	if err != nil {
		return nil, errors.Wrap(err, "parsing yaml")
	}
	return &config, nil
}
