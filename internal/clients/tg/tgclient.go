package tg

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
)

type TokenGetter interface {
	Token() string
}

type Client struct {
	client *tgbotapi.BotAPI
}

func New(tokenGetter TokenGetter) (*Client, error) {
	client, err := tgbotapi.NewBotAPI(tokenGetter.Token())

	if err != nil {
		return nil, errors.Wrap(err, "NewBotApi")
	}

	return &Client{
		client: client,
	}, nil
}

func (c *Client) SendMessage(text string, userID int64) error {
	_, err := c.client.Send(tgbotapi.NewMessage(userID, text))
	if err != nil {
		return errors.Wrap(err, "client.Send")
	}
	return nil
}

func (c *Client) GetIncomingChannel() <-chan messages.Message {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	in := c.client.GetUpdatesChan(u)
	out := make(chan messages.Message)

	go func() {
		defer close(out)
		for tgMessage := range in {
			if tgMessage.Message != nil { // If we got a message
				out <- messages.Message{
					Text:   tgMessage.Message.Text,
					UserID: expense.UserId(tgMessage.Message.From.ID),
				}
			}
		}
	}()

	return out
}

func (c *Client) StopReceivingUpdates() {
	c.client.StopReceivingUpdates()
}
