package metrics

import (
	"context"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	"net/http"
	"time"
)

const (
	shutdownTimeoutMilliSeconds = 100
	defaultPort                 = 8080
)

var (
	InFlightRequests = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "in_flight_requests_total",
	})
	Requests = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "requests_total",
	})
	HistogramResponseTime = promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "ozon",
			Subsystem: "bot",
			Name:      "histogram_response_time_seconds",
			Buckets:   prometheus.ExponentialBucketsRange(0.0001, 1, 16),
		},
	)
	CommandsCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "commands_total",
	},
		[]string{"command"},
	)
	HistogramCommandResponseTime = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "ozon",
			Subsystem: "bot",
			Name:      "histogram_command_response_time_seconds",
			Buckets:   prometheus.ExponentialBucketsRange(0.0001, 1, 16),
		},
		[]string{"command"},
	)

	Rates = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "ozon",
			Subsystem: "bot",
			Name:      "rates",
		},
		[]string{"currency"},
	)
	RateUpdates = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "rate_updates_total",
	})
	RateUpdatesErrors = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "rate_updates_errors_total",
	})
	SummaryRateUpdateTime = promauto.NewSummary(
		prometheus.SummaryOpts{
			Namespace:  "ozon",
			Subsystem:  "bot",
			Name:       "summary_rate_update_time_seconds",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
	)
)

func Run(ctx context.Context, port int, log *zap.Logger) {
	if port == 0 {
		port = defaultPort
	}
	log.Info("Metrics server starting", zap.Int("port", port))

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	httpServer := http.Server{
		Handler: mux,
		Addr:    fmt.Sprintf(":%d", port),
	}

	go func() {
		err := httpServer.ListenAndServe()
		if err != http.ErrServerClosed {
			log.Error("Metrics server error", zap.Error(err))
			return
		}
		log.Info("Metrics server stopped")
	}()

	go func() {
		<-ctx.Done()
		shutdownCtx, shutdownRelease := context.WithTimeout(
			context.Background(),
			shutdownTimeoutMilliSeconds*time.Millisecond,
		)
		defer shutdownRelease()

		if err := httpServer.Shutdown(shutdownCtx); err != nil {
			log.Error("Metrics server shutdown error", zap.Error(err))
		}
		log.Info("Metrics server graceful shutdown")
	}()
}

func SetCurrencyRate(currency string, rate float64) {
	Rates.
		With(prometheus.Labels{"currency": currency}).
		Set(rate)
}
