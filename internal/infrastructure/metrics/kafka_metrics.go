package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	HistogramKafkaSendTime = promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "ozon",
			Subsystem: "kafka",
			Name:      "histogram_kafka_send_time_seconds",
			Buckets:   prometheus.ExponentialBucketsRange(0.0001, 1, 16),
		},
	)
	HistogramKafkaConsumeTime = promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "ozon",
			Subsystem: "kafka",
			Name:      "histogram_kafka_consume_time_seconds",
			Buckets:   prometheus.ExponentialBucketsRange(0.0001, 1, 16),
		},
	)
)
