package metrics

import (
	"github.com/DmitriyVTitov/size"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	CacheMissed = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "cache_missed_total",
	},
	)
	CacheHits = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "cache_hits_total",
	},
	)
	CacheKeys = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "cache_keys_total",
	},
	)
	CacheSize = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "ozon",
		Subsystem: "bot",
		Name:      "cache_size_total_bytes",
	},
	)
)

type Cache interface {
	Set(key string, value any)
	Get(key string) any
	Remove(key string) bool
	Len() int
}

type CacheWithMetrics struct {
	inner Cache
}

func NewCacheWithMetrics(inner Cache) *CacheWithMetrics {
	return &CacheWithMetrics{inner: inner}
}

func (c *CacheWithMetrics) Set(key string, value any) {
	c.inner.Set(key, value)
	CacheKeys.Set(float64(c.inner.Len()))
	CacheSize.Set(float64(size.Of(c.inner)))
}

func (c *CacheWithMetrics) Get(key string) any {
	value := c.inner.Get(key)
	if value == nil {
		CacheMissed.Inc()
	} else {
		CacheHits.Inc()
	}
	return value
}

func (c *CacheWithMetrics) Remove(key string) bool {
	result := c.inner.Remove(key)
	CacheKeys.Set(float64(c.inner.Len()))
	CacheSize.Set(float64(size.Of(c.inner)))
	return result
}

func (c *CacheWithMetrics) Len() int {
	return c.inner.Len()
}
