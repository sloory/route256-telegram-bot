package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	HistogramGRPCResponseTime = promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "ozon",
			Subsystem: "grpc",
			Name:      "histogram_grpc_call_time_seconds",
			Buckets:   prometheus.ExponentialBucketsRange(0.0001, 1, 16),
		},
	)
)
