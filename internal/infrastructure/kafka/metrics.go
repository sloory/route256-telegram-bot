package kafka

import (
	"context"
	"github.com/deathowl/go-metrics-prometheus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rcrowley/go-metrics"
	"go.uber.org/zap"
	"time"
)

const (
	updatePeriodInSec = 1
)

func RunMetrics(ctx context.Context, registry metrics.Registry, log *zap.Logger) {
	prometheusProvider := prometheusmetrics.NewPrometheusProvider(
		registry,
		"ozon",
		"kafka",
		prometheus.DefaultRegisterer,
		updatePeriodInSec*time.Second,
	)

	go func() {
		updateTicker := time.NewTicker(prometheusProvider.FlushInterval)
	stopUpdate:
		for {
			select {
			case <-updateTicker.C:
				err := prometheusProvider.UpdatePrometheusMetricsOnce()
				if err != nil {
					log.Error("update kafka metrics error", zap.Error(err))
				}
			case <-ctx.Done():
				break stopUpdate
			}
		}
		updateTicker.Stop()
	}()
}
