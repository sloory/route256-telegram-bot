package consumer

import (
	"github.com/Shopify/sarama"
	"go.uber.org/zap"
)

type LogInterceptor struct {
	log *zap.Logger
}

func NewLogInterceptor(log *zap.Logger) *LogInterceptor {
	return &LogInterceptor{log: log}
}

func (c *LogInterceptor) OnConsume(msg *sarama.ConsumerMessage) {
	c.log.Info(
		"New message received",
		zap.String("topic", msg.Topic),
		zap.Int64("offset", msg.Offset),
		zap.Int32("partition", msg.Partition),
		zap.String("key", string(msg.Key)),
		zap.String("value", string(msg.Value)),
	)
}
