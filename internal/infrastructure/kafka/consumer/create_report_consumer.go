package consumer

import (
	"context"
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/grpc/gen/proto/go/api/v1"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"time"
)

type Reporter interface {
	ReportStartFrom(context.Context, expense.UserId, time.Time) (expense.Report, error)
}

type ReportApiClient interface {
	Send(ctx context.Context, request *api.CreatedRequest) error
}

type CreateReportConsumer struct {
	reporter  Reporter
	apiClient ReportApiClient
}

func NewCreateReportConsumer(reporter Reporter, apiClient ReportApiClient) *CreateReportConsumer {
	return &CreateReportConsumer{reporter: reporter, apiClient: apiClient}
}

func (c *CreateReportConsumer) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *CreateReportConsumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *CreateReportConsumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for {
		select {
		case message := <-claim.Messages():
			startTime := time.Now()

			createReportMsg, err := kafka.DecodeCreateReportMessage(message.Value)
			if err != nil {
				return err
			}

			report, err := c.reporter.ReportStartFrom(
				session.Context(),
				expense.UserId(createReportMsg.UserId),
				createReportMsg.StartDate,
			)

			if err != nil {
				return err
			}

			var expenses []*api.CreatedRequest_CategoryExpense
			for category, expenseValue := range report {
				expenses = append(
					expenses,
					&api.CreatedRequest_CategoryExpense{
						Name:    category,
						Expense: uint32(expenseValue.Value),
					},
				)
			}
			request := api.CreatedRequest{
				UserId:      createReportMsg.UserId,
				PeriodTitle: "all time",
				Expenses:    expenses,
			}
			err = c.apiClient.Send(session.Context(), &request)
			if err != nil {
				return err
			}

			session.MarkMessage(message, "")

			duration := time.Since(startTime)
			metrics.HistogramKafkaConsumeTime.Observe(duration.Seconds())

		case <-session.Context().Done():
			return nil
		}
	}
}
