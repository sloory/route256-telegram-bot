package consumer

import (
	"context"
	_ "context"
	"github.com/Shopify/sarama"
	_ "github.com/Shopify/sarama"
	_ "github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka/producer"
	"go.uber.org/zap"
	_ "go.uber.org/zap"
)

const (
	KafkaConsumerGroupName = "create-report-group"
)

func NewConsumerConfig() *sarama.Config {
	consumerConfig := sarama.NewConfig()
	consumerConfig.Version = sarama.V2_8_2_0
	consumerConfig.Consumer.Offsets.Initial = sarama.OffsetOldest
	consumerConfig.Consumer.Return.Errors = true
	consumerConfig.Consumer.Group.Rebalance.GroupStrategies = []sarama.BalanceStrategy{sarama.BalanceStrategyRange}
	return consumerConfig
}

type CreateReportConsumerGroup struct {
	brokerList []string
	config     *sarama.Config
	log        *zap.Logger
}

func NewCreateReportConsumerGroup(brokerList []string, config *sarama.Config, log *zap.Logger) *CreateReportConsumerGroup {
	return &CreateReportConsumerGroup{brokerList: brokerList, config: config, log: log}
}

func (c *CreateReportConsumerGroup) Start(
	ctx context.Context,
	consumer sarama.ConsumerGroupHandler,
) error {
	group, err := sarama.NewConsumerGroup(c.brokerList, KafkaConsumerGroupName, c.config)
	if err != nil {
		return err
	}

	defer func() {
		err := group.Close()
		if err != nil {
			c.log.Error("close kafka group error", zap.Error(err))
		}
	}()

	// Track errors
	go func() {
		for err := range group.Errors() {
			c.log.Error("kafka consumer group", zap.Error(err))
		}
	}()

	for {
		err = group.Consume(ctx, []string{producer.Topic}, consumer)
		if err != nil {
			return err
		}

		select {
		case <-ctx.Done():
			return nil
		default:
			// continue work. we have deal with kafka rebalance
		}
	}
}
