package kafka

import (
	"encoding/json"
	"time"
)

type CreateReportMessage struct {
	UserId    int64
	StartDate time.Time
}

func DecodeCreateReportMessage(value []byte) (*CreateReportMessage, error) {
	var msg CreateReportMessage
	err := json.Unmarshal(value, &msg)

	if err != nil {
		return nil, err
	}
	return &msg, nil
}

func (m CreateReportMessage) Encode() ([]byte, error) {
	return json.Marshal(m)
}

func (m CreateReportMessage) Length() int {
	data, _ := m.Encode()
	return len(data)
}
