package producer

import (
	"encoding/json"
	"github.com/Shopify/sarama/mocks"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
	"testing"
	"time"
)

func Test_SendMessage(t *testing.T) {
	sp := mocks.NewSyncProducer(t, nil)
	defer func() {
		if err := sp.Close(); err != nil {
			t.Error(err)
		}
	}()

	userId := int64(1)
	startDate := time.Now()

	sp = sp.ExpectSendMessageWithCheckerFunctionAndSucceed(
		func(val []byte) error {
			println(string(val))
			var msg kafka.CreateReportMessage
			err := json.Unmarshal(val, &msg)
			if err != nil {
				return err
			}

			assert.Equal(t, userId, msg.UserId)
			assert.True(t, startDate.Equal(msg.StartDate))
			return nil
		},
	)

	createReport := NewCreateReportMsgSender(sp, tests.Log)
	err := createReport.Send(userId, startDate)
	assert.NoError(t, err)
}
