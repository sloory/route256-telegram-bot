package producer

import (
	"github.com/Shopify/sarama"
	"go.uber.org/zap"
)

type LogInterceptor struct {
	log *zap.Logger
}

func NewLogInterceptor(log *zap.Logger) *LogInterceptor {
	return &LogInterceptor{log: log}
}

func (c *LogInterceptor) OnSend(msg *sarama.ProducerMessage) {
	value, err := msg.Value.Encode()
	if err != nil {
		c.log.Error(
			"kafka msg encode err", zap.Error(err),
		)
	} else {
		c.log.Info(
			"kafka message send",
			zap.String("value", string(value)),
		)
	}
}
