package producer

import (
	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/kafka"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	"go.uber.org/zap"
	"time"
)

const (
	Topic = "bot-reports"
)

type CreateReportMsgSender struct {
	producer sarama.SyncProducer
	log      *zap.Logger
}

func NewCreateReportMsgSender(producer sarama.SyncProducer, log *zap.Logger) *CreateReportMsgSender {
	return &CreateReportMsgSender{producer: producer, log: log}
}

func (m *CreateReportMsgSender) Send(userId int64, startDate time.Time) error {

	msg := sarama.ProducerMessage{
		Topic: Topic,
		Value: kafka.CreateReportMessage{
			UserId:    userId,
			StartDate: startDate,
		},
	}

	startTime := time.Now()

	_, _, err := m.producer.SendMessage(&msg)

	duration := time.Since(startTime)
	metrics.HistogramKafkaSendTime.Observe(duration.Seconds())

	if err != nil {
		m.log.Error("failed to send kafka message", zap.Error(err))
		return err
	}

	return nil
}
