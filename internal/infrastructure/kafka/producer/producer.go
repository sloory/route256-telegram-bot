package producer

import (
	_ "context"
	"github.com/Shopify/sarama"
	_ "github.com/Shopify/sarama"
	_ "github.com/pkg/errors"
	_ "go.uber.org/zap"
	"time"
)

var (
	producerConfig *sarama.Config
)

func GetProducerConfig() *sarama.Config {
	if producerConfig == nil {
		producerConfig = sarama.NewConfig()
		producerConfig.Version = sarama.V2_8_2_0
		producerConfig.Producer.RequiredAcks = sarama.WaitForLocal
		producerConfig.Producer.Retry.Max = 3
		producerConfig.Producer.Retry.Backoff = time.Millisecond * 250
		//  Successfully delivered messages will be returned on the Successes channe
		producerConfig.Producer.Return.Successes = true
		// Generates partitioners for choosing the partition to send messages to (defaults to hashing the message key)
		_ = producerConfig.Producer.Partitioner
	}

	return producerConfig
}
