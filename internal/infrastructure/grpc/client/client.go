package apiClient

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/grpc/gen/proto/go/api/v1"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/metrics"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"time"
)

const (
	defaultPort = 8082
)

type ReportServiceClient struct {
	port   int
	conn   *grpc.ClientConn
	client api.ReportServiceClient
}

func NewReportServiceClient(port int) *ReportServiceClient {
	if port == 0 {
		port = defaultPort
	}
	return &ReportServiceClient{port: port}
}

func (c *ReportServiceClient) Connect(ctx context.Context) error {
	conn, err := grpc.Dial(
		fmt.Sprintf(":%d", c.port),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(MetricsUnaryInterceptor),
	)
	if err != nil {
		return err
	}

	c.conn = conn
	c.client = api.NewReportServiceClient(c.conn)
	return nil
}

func (c *ReportServiceClient) Close() error {
	if c.conn == nil {
		return nil
	}

	return c.conn.Close()
}

func (c *ReportServiceClient) Send(ctx context.Context, request *api.CreatedRequest) error {
	_, err := c.client.Created(ctx, request)
	return err
}

func MetricsUnaryInterceptor(
	ctx context.Context, method string, req interface{},
	reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption,
) error {
	startTime := time.Now()
	err := invoker(ctx, method, req, reply, cc, opts...)
	duration := time.Since(startTime)
	metrics.HistogramGRPCResponseTime.Observe(duration.Seconds())
	return err
}
