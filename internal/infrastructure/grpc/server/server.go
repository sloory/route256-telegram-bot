package apiServer

import (
	"context"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	api "gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/grpc/gen/proto/go/api/v1"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/messages"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"net"
	"net/http"
	"time"
)

const (
	shutdownTimeoutMilliSeconds = 100
	defaultHttpPort             = 8081
	defaultGRPCPort             = 8082
)

type MessageSender interface {
	SendMessage(text string, userID int64) error
}

type ReportServiceServer struct {
	log       *zap.Logger
	msgSender MessageSender
	api.UnimplementedReportServiceServer
}

func (s *ReportServiceServer) Created(ctx context.Context, request *api.CreatedRequest) (*api.CreatedResponse, error) {
	err := request.Validate()
	if err != nil {
		return nil, err
	}

	report := make(expense.Report)
	for _, ex := range request.Expenses {
		report[ex.Name] = money.Money{Value: int(ex.Expense), Currency: money.RUB}
	}

	s.log.Info(
		"Api receive report",
		zap.String("report", fmt.Sprint(request.Expenses)),
		zap.String("period", request.PeriodTitle),
		zap.Int64("userId", request.UserId),
	)

	reportResponse := messages.ReportResponse{Report: report, PeriodTitle: "all"}
	err = s.msgSender.SendMessage(reportResponse.String(), request.UserId)
	if err != nil {
		s.log.Error("error while sent report to telegram", zap.Error(err))
		return nil, status.Errorf(codes.Internal, "send report to telegram error")
	}

	return &api.CreatedResponse{}, nil

}

func RunGRPC(ctx context.Context, port int, msgSender MessageSender, log *zap.Logger) {
	if port == 0 {
		port = defaultGRPCPort
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal("failed to listen grpc port", zap.Error(err))
	}

	server := grpc.NewServer()
	api.RegisterReportServiceServer(
		server,
		&ReportServiceServer{
			msgSender: msgSender,
			log:       log,
		},
	)

	go func(server *grpc.Server) {
		log.Info("grps server started", zap.Int("port", port))
		if err := server.Serve(lis); err != nil {
			log.Fatal("failed to serve grpc server", zap.Error(err))
		}
	}(server)

	go func() {
		<-ctx.Done()
		server.GracefulStop()
	}()
}

func RunHttp(ctx context.Context, port int, grpcPort int, log *zap.Logger) {
	if port == 0 {
		port = defaultHttpPort
	}

	if grpcPort == 0 {
		grpcPort = defaultGRPCPort
	}

	mux := runtime.NewServeMux()
	err := api.RegisterReportServiceHandlerFromEndpoint(
		ctx,
		mux,
		fmt.Sprintf(":%d", grpcPort),
		[]grpc.DialOption{
			grpc.WithTransportCredentials(insecure.NewCredentials()),
		},
	)
	if err != nil {
		log.Fatal("RegisterReportServiceHandlerFromEndpoint", zap.Error(err))
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal("failed to listen http api port", zap.Error(err))
	}

	server := http.Server{Handler: mux}

	go func(server *http.Server) {
		log.Info("http api server started", zap.Int("port", port))
		if err := server.Serve(lis); err != nil {
			log.Fatal("failed to serve http api server", zap.Error(err))
		}
	}(&server)

	go func() {
		<-ctx.Done()
		shutdownCtx, shutdownRelease := context.WithTimeout(
			context.Background(),
			shutdownTimeoutMilliSeconds*time.Millisecond,
		)
		defer shutdownRelease()

		if err := server.Shutdown(shutdownCtx); err != nil {
			log.Error("http api server shutdown error", zap.Error(err))
		}
		log.Info("http api server graceful shutdown")
	}()
}
