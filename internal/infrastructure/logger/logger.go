package logger

import (
	"log"

	"go.uber.org/zap"
)

func InitLogger(devMode bool) *zap.Logger {
	var logger *zap.Logger
	var err error
	if devMode {
		logger, err = zap.NewDevelopment()
	} else {
		cfg := zap.NewProductionConfig()
		cfg.DisableCaller = true
		cfg.DisableStacktrace = true
		cfg.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
		logger, err = cfg.Build()
	}

	if err != nil {
		log.Fatal("cannot init zap", err)
		return nil
	}

	return logger
}
