package expenseDatabase

import (
	"testing"

	_ "github.com/lib/pq"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
)

func Test_DB_SumIsZeroIfUserNotHaveExpense(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSumIsZeroIfUserNotHaveExpense(t, repo)
}

func Test_DB_SumForAllUserExpense(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSumForAllUserExpense(t, repo)
}

func Test_DB_SumForLastWeek(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSumForLastWeek(t, repo)
}

func Test_DB_SumForAllTime(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSumForAllTime(t, repo)
}

func Test_DB_SumByDifferentCategories(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSumByDifferentCategories(t, repo)
}

func Test_DB_SumStartFromIncludeStartDateExpense(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSumStartFromIncludeStartDateExpense(t, repo)
}

func Test_DB_ClearUserExpenses(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestClearUserExpenses(t, repo)
}

func Test_DB_ExpanseStartFromIfUserNotHaveExpense(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestExpanseStartFromIfUserNotHaveExpense(t, repo)
}

func Test_DB_ExpanseStartFrom(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestExpanseStartFrom(t, repo)
}
