package expenseDatabase

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_SafeReportStartFrom(t *testing.T) {
	repo := NewThreadSafeExpenseStorage(NewInMemoryRepository())
	expenseObj, _ := expense.NewTodayExpense(
		money.NewRub(100, 0),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(100, 0), report["Еда"])
}

func Test_SafeClear(t *testing.T) {
	repo := NewThreadSafeExpenseStorage(NewInMemoryRepository())
	expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 0), "Еда")
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	err = repo.Clear(context.Background(), expense.UserId(1))
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.NoError(t, err)
	assert.Equal(t, expense.Report{}, report)
}

func Test_ThreadSafeTest(t *testing.T) {
	repo := NewThreadSafeExpenseStorage(NewInMemoryRepository())
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		for i := 0; i < 100; i++ {
			expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 0), "Еда")
			_ = repo.Add(context.Background(), expense.UserId(1), expenseObj)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < 100; i++ {
			_, _ = repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < 100; i++ {
			_, _ = repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Time{})
		}
		wg.Done()
	}()

	wg.Wait()
}
