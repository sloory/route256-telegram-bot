package expenseDatabase_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
	"sync"
	"testing"
	"time"
)

var testCases = []struct {
	transactor expense.Transactor
	locker     expenseDatabase.Locker
}{
	{
		transactor: tests.GetTestDB(),
		locker:     expenseDatabase.NewDBExpenseLocker(tests.GetTestDB(), tests.Log),
	},
	{
		transactor: &database.FakeTransactor{},
		locker:     &expenseDatabase.MutexExpenseLocker{},
	},
}

func Test_LockNotLocked(t *testing.T) {
	for _, testCase := range testCases {
		locker := testCase.locker
		transactor := testCase.transactor
		ctx := context.Background()

		syncTx := make(chan bool)
		wg := sync.WaitGroup{}
		wg.Add(2)
		go func() {
			err := transactor.WithinTransaction(ctx, func(ctx context.Context) error {
				unlock := locker.Lock(ctx, expense.UserId(1))
				syncTx <- true
				<-syncTx

				unlock()
				return nil
			})
			assert.NoError(t, err)
			wg.Done()
		}()

		go func() {
			err := transactor.WithinTransaction(ctx, func(ctx context.Context) error {
				<-syncTx
				unlock := locker.Lock(ctx, expense.UserId(2))
				unlock()
				syncTx <- true
				close(syncTx)
				return nil
			})
			assert.NoError(t, err)
			wg.Done()
		}()

		waitResult := waitTimeout(&wg, 200*time.Millisecond)
		assert.True(t, waitResult)
	}
}

func Test_LockLocked(t *testing.T) {
	for _, testCase := range testCases {
		locker := testCase.locker
		transactor := testCase.transactor
		ctx := context.Background()

		syncTx := make(chan bool)
		results := make(chan bool)
		wg := sync.WaitGroup{}
		wg.Add(2)
		go func() {
			err := transactor.WithinTransaction(ctx, func(ctx context.Context) error {
				unlock := locker.Lock(ctx, expense.UserId(1))
				syncTx <- true
				close(syncTx)
				tm := time.NewTimer(100 * time.Millisecond)

				var result bool
				select {
				case result = <-results:
				case <-tm.C:
					result = false
				}
				assert.False(t, result)
				tm.Stop()

				unlock()
				return nil
			})

			assert.NoError(t, err)

			tm := time.NewTimer(100 * time.Millisecond)
			var result bool
			select {
			case result = <-results:
			case <-tm.C:
				result = false
			}
			tm.Stop()
			assert.True(t, result)

			wg.Done()
		}()

		go func() {
			err := transactor.WithinTransaction(ctx, func(ctx context.Context) error {
				<-syncTx
				unlock := locker.Lock(ctx, expense.UserId(1))
				results <- true
				close(results)

				unlock()
				return nil
			})
			assert.NoError(t, err)
			wg.Done()
		}()

		waitResult := waitTimeout(&wg, 200*time.Millisecond)
		assert.True(t, waitResult)
	}
}

func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	select {
	case <-c:
		return true // completed normally
	case <-time.After(timeout):
		return false // timed out
	}
}
