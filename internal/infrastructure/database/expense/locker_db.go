package expenseDatabase

import (
	"context"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"go.uber.org/zap"
)

type DBExpenseLocker struct {
	db  *database.DB
	log *zap.Logger
}

func NewDBExpenseLocker(db *database.DB, log *zap.Logger) *DBExpenseLocker {
	return &DBExpenseLocker{db, log}
}

const addExpenseLockId = 1

func (l *DBExpenseLocker) Lock(ctx context.Context, id expense.UserId) func() {

	const query = "SELECT pg_advisory_xact_lock($1, $2);"

	_, err := l.db.ExecContext(ctx, query, addExpenseLockId, id)
	if err != nil {
		l.log.Fatal("Error while take db lock", zap.Error(err))
	}

	return func() {
		//nothing to do - lock will be release after transaction finished
	}
}
