package expenseDatabase

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func TestSumIsZeroIfUserNotHaveExpense(t *testing.T, repo ExpenseStorage) {
	expenseObj, _ := expense.NewTodayExpense(
		money.NewRub(1, 0),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(2), time.Now().AddDate(0, 0, -1))

	assert.NoError(t, err)
	assert.Equal(t, expense.Report{}, report)
}

func TestSumForAllUserExpense(t *testing.T, repo ExpenseStorage) {
	expenseObj, _ := expense.NewTodayExpense(
		money.NewRub(100, 0),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewTodayExpense(
		money.NewRub(200, 0),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewTodayExpense(
		money.NewRub(400, 0),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(2), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(300, 0), report["Еда"])
}

func TestSumForLastWeek(t *testing.T, repo ExpenseStorage) {

	expenseObj, _ := expense.NewExpense(
		money.NewRub(100, 0),
		time.Now().AddDate(0, 0, -1),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(
		money.NewRub(200, 0),
		time.Now().AddDate(0, 0, -3),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(
		money.NewRub(300, 0),
		time.Now().AddDate(0, 0, -4),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(
		money.NewRub(700, 0),
		time.Now().AddDate(0, 0, -10),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -7))
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(600, 0), report["Еда"])
}

func TestSumForAllTime(t *testing.T, repo ExpenseStorage) {
	expenseObj, _ := expense.NewExpense(
		money.NewRub(100, 0),
		time.Now().AddDate(0, 0, -1),
		"Еда",
	)
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(
		money.NewRub(200, 0),
		time.Now().AddDate(0, -10, 0),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(
		money.NewRub(300, 0),
		time.Now().AddDate(-10, 0, 0),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Time{})
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(600, 0), report["Еда"])
}

func TestSumByDifferentCategories(t *testing.T, repo ExpenseStorage) {
	expenseObj, _ := expense.NewExpense(money.NewRub(100, 0), time.Now(), "Еда")
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(money.NewRub(200, 0), time.Now(), "Еда")
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewExpense(money.NewRub(400, 0), time.Now(), "Ипотека")
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.NoError(t, err)
	assert.Equal(
		t,
		expense.Report{
			"Еда":     money.NewRub(300, 0),
			"Ипотека": money.NewRub(400, 0),
		},
		report,
	)
}

func TestSumStartFromIncludeStartDateExpense(t *testing.T, repo ExpenseStorage) {
	startFrom := time.Now().AddDate(0, 0, -1)
	expenseObj, _ := expense.NewExpense(money.NewRub(100, 0), startFrom, "Еда")
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), startFrom)
	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(100, 0), report["Еда"])
}

func TestClearUserExpenses(t *testing.T, repo ExpenseStorage) {
	expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 0), "Еда")
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	expenseObj, _ = expense.NewTodayExpense(money.NewRub(200, 0), "Еда")
	err = repo.Add(context.Background(), expense.UserId(2), expenseObj)
	assert.NoError(t, err)

	err = repo.Clear(context.Background(), expense.UserId(1))
	assert.NoError(t, err)

	report, err := repo.ReportStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))
	assert.NoError(t, err)
	assert.Equal(t, expense.Report{}, report)

	report, err = repo.ReportStartFrom(context.Background(), expense.UserId(2), time.Now().AddDate(0, 0, -1))
	assert.NoError(t, err)
	assert.NotEqual(t, expense.Report{}, report)
}

func TestExpanseStartFromIfUserNotHaveExpense(t *testing.T, repo ExpenseStorage) {
	sum, err := repo.ExpenseStartFrom(context.Background(), expense.UserId(1), time.Now().AddDate(0, 0, -1))

	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(0, 0), sum)
}

func TestExpanseStartFrom(t *testing.T, repo ExpenseStorage) {

	// today and needed user
	expenseObj, _ := expense.NewTodayExpense(money.NewRub(100, 1), "Еда")
	err := repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	// needed user but not today
	expenseObj, _ = expense.NewExpense(
		money.NewRub(200, 0),
		time.Now().AddDate(0, -10, 0),
		"Еда",
	)
	err = repo.Add(context.Background(), expense.UserId(1), expenseObj)
	assert.NoError(t, err)

	// today but another user
	expenseObj, _ = expense.NewTodayExpense(money.NewRub(200, 0), "Еда")
	err = repo.Add(context.Background(), expense.UserId(2), expenseObj)
	assert.NoError(t, err)

	sum, err := repo.ExpenseStartFrom(
		context.Background(),
		expense.UserId(1),
		time.Now().AddDate(0, 0, -1),
	)

	assert.NoError(t, err)
	assert.Equal(t, money.NewRub(100, 1), sum)
}
