package expenseDatabase

import (
	"context"
	"sync"
	"time"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type ThreadSafeExpenseStorage struct {
	inner ExpenseStorage
	mutex sync.RWMutex
}

func NewThreadSafeExpenseStorage(inner ExpenseStorage) *ThreadSafeExpenseStorage {
	return &ThreadSafeExpenseStorage{inner: inner, mutex: sync.RWMutex{}}
}

func (r *ThreadSafeExpenseStorage) Add(ctx context.Context, id expense.UserId, expense *expense.Expense) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.inner.Add(ctx, id, expense)
}

func (r *ThreadSafeExpenseStorage) Clear(ctx context.Context, id expense.UserId) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.inner.Clear(ctx, id)
}

func (r *ThreadSafeExpenseStorage) ReportStartFrom(ctx context.Context, id expense.UserId, time time.Time) (expense.Report, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	return r.inner.ReportStartFrom(ctx, id, time)
}

func (r *ThreadSafeExpenseStorage) ExpenseStartFrom(ctx context.Context, id expense.UserId, start time.Time) (money.Money, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	return r.inner.ExpenseStartFrom(ctx, id, start)
}
