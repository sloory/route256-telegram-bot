package expenseDatabase

import (
	"context"
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type DBRepository struct {
	db *database.DB
}

func NewDBRepository(db *database.DB) *DBRepository {
	return &DBRepository{db}
}

func (r *DBRepository) Add(ctx context.Context, id expense.UserId, expense *expense.Expense) error {
	categoryId, err := r.getOrCreateCategoryId(ctx, expense.Category)
	if err != nil {
		return err
	}

	const query = `
		insert into expenses(
			user_id,
			category_id, 
			amount,
			datetime
		) values (
			$1, $2, $3, $4
		);
`
	_, err = r.db.ExecContext(ctx, query, id, categoryId, expense.Amount.Value, expense.Date)
	if err != nil {
		return err
	}
	return nil
}

func (r *DBRepository) Clear(ctx context.Context, id expense.UserId) error {
	_, err := r.db.ExecContext(ctx, "delete from expenses where user_id =$1", id)
	if err != nil {
		return err
	}

	return nil
}

func (r *DBRepository) ReportStartFrom(ctx context.Context, id expense.UserId, startFrom time.Time) (expense.Report, error) {
	query := `
		select categories.name, sum(expenses.amount) as amount from
			expenses, categories
		where
		    expenses.category_id = categories.id
		  	and expenses.user_id = $1
`
	zeroTime := time.Time{}
	if startFrom != zeroTime {
		query += " and expenses.datetime >= $2"
	}

	query += " group by categories.name"

	var rows *sql.Rows
	var err error
	if startFrom != zeroTime {
		rows, err = r.db.QueryContext(ctx, query, id, startFrom)
	} else {
		rows, err = r.db.QueryContext(ctx, query, id)
	}

	if err != nil {
		return nil, errors.Wrap(err, "error while build report")
	}
	defer rows.Close()

	return r.buildReportByRows(rows)
}

func (r *DBRepository) ExpenseStartFrom(ctx context.Context, id expense.UserId, time time.Time) (money.Money, error) {
	const query = `
		select sum(expenses.amount) from expenses
		where
		  	expenses.user_id = $1
			and expenses.datetime >= $2
`
	var value sql.NullInt64
	var sum int
	err := r.db.QueryRowContext(ctx, query, id, time).Scan(&value)
	if err != nil {
		return money.NewRub(0, 0), errors.Wrap(err, "error while set expense")
	}

	if value.Valid {
		sum = int(value.Int64)
	} else {
		sum = 0
	}

	return money.Money{Value: sum, Currency: money.RUB}, nil
}

func (r *DBRepository) getOrCreateCategoryId(ctx context.Context, category string) (int64, error) {

	id, err := r.getCategoryIdByName(ctx, category)
	if err == nil {
		return id, nil
	}

	if err != sql.ErrNoRows {
		return 0, errors.Wrap(err, "error while get category")
	}

	const insertQuery = `
insert into categories (name) values ($1) on conflict (name) do nothing
	`
	_, err = r.db.ExecContext(ctx, insertQuery, category)
	if err != nil {
		return 0, errors.Wrap(err, "error while insert category")
	}
	return r.getCategoryIdByName(ctx, category)
}

func (r *DBRepository) getCategoryIdByName(ctx context.Context, category string) (int64, error) {
	const getQuery = "select id from categories where name = $1"
	var id int64
	err := r.db.QueryRowContext(ctx, getQuery, category).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (r *DBRepository) buildReportByRows(rows *sql.Rows) (expense.Report, error) {
	report := make(expense.Report)
	var category string
	var amount int
	for rows.Next() {
		err := rows.Scan(&category, &amount)
		if err != nil {
			return nil, errors.Wrap(err, "error while build report")
		}
		report[category] = money.Money{Value: amount, Currency: money.RUB}
	}

	return report, nil
}
