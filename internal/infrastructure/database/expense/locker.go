package expenseDatabase

import (
	"context"
	"sync"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
)

type Locker interface {
	Lock(ctx context.Context, id expense.UserId) func()
}

type MutexExpenseLocker struct {
	mutexes sync.Map
}

func (m *MutexExpenseLocker) Lock(ctx context.Context, id expense.UserId) func() {
	value, _ := m.mutexes.LoadOrStore(id, &sync.Mutex{})
	mtx := value.(*sync.Mutex)
	mtx.Lock()

	return func() { mtx.Unlock() }
}
