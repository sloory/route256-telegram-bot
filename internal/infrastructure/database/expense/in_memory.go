package expenseDatabase

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type ExpenseStorage interface {
	Add(context.Context, expense.UserId, *expense.Expense) error
	Clear(context.Context, expense.UserId) error
	ReportStartFrom(context.Context, expense.UserId, time.Time) (expense.Report, error)
	ExpenseStartFrom(context.Context, expense.UserId, time.Time) (money.Money, error)
}

var ErrCanStoreNotRubbleExpense = errors.New("can not store not rubble expense")

type InMemoryRepository struct {
	data map[expense.UserId][]*expense.Expense
}

func NewInMemoryRepository() *InMemoryRepository {
	return &InMemoryRepository{
		data: make(map[expense.UserId][]*expense.Expense),
	}
}

func (r *InMemoryRepository) Add(ctx context.Context, id expense.UserId, expense *expense.Expense) error {
	r.data[id] = append(r.data[id], expense)
	return nil
}

func (r *InMemoryRepository) Clear(ctx context.Context, id expense.UserId) error {
	delete(r.data, id)
	return nil
}

func (r *InMemoryRepository) ReportStartFrom(ctx context.Context, id expense.UserId, time time.Time) (expense.Report, error) {
	report := make(expense.Report)
	list, ok := r.data[id]
	if !ok {
		return report, nil
	}

	for _, expenseObj := range list {
		if expenseObj.Date.After(time) || expenseObj.Date.Equal(time) {
			expense, ok := report[expenseObj.Category]
			if !ok {
				expense = money.New(0, 0, money.RUB)

			}
			expense.Value += expenseObj.Amount.Value
			report[expenseObj.Category] = expense
		}
	}
	return report, nil
}

func (r *InMemoryRepository) ExpenseStartFrom(ctx context.Context, id expense.UserId, time time.Time) (money.Money, error) {
	list, ok := r.data[id]
	if !ok {
		return money.NewRub(0, 0), nil
	}

	value := 0
	for _, expenseObj := range list {
		if expenseObj.Date.After(time) || expenseObj.Date.Equal(time) {
			value += expenseObj.Amount.Value
		}
	}

	return money.Money{Value: value, Currency: money.RUB}, nil
}
