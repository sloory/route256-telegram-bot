package expenseDatabase

import (
	"testing"
)

func Test_SumIsZeroIfUserNotHaveExpense(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSumIsZeroIfUserNotHaveExpense(t, repo)
}

func Test_SumForAllUserExpense(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSumForAllUserExpense(t, repo)
}

func Test_SumForLastWeek(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSumForLastWeek(t, repo)
}

func Test_SumForAllTime(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSumForAllTime(t, repo)
}

func Test_SumByDifferentCategories(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSumByDifferentCategories(t, repo)
}

func Test_SumStartFromIncludeStartDateExpense(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSumStartFromIncludeStartDateExpense(t, repo)
}

func Test_ClearUserExpenses(t *testing.T) {
	repo := NewInMemoryRepository()
	TestClearUserExpenses(t, repo)
}

func Test_ExpanseStartFromIfUserNotHaveExpense(t *testing.T) {
	repo := NewInMemoryRepository()
	TestExpanseStartFromIfUserNotHaveExpense(t, repo)
}

func Test_ExpanseStartFrom(t *testing.T) {
	repo := NewInMemoryRepository()
	TestExpanseStartFrom(t, repo)
}
