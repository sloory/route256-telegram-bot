package database

import "context"

type FakeTransactor struct {
}

func (t *FakeTransactor) WithinTransaction(ctx context.Context, tFunc func(ctx context.Context) error) error {
	return tFunc(ctx)
}
