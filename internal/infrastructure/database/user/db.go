package expenseDatabase

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type DBRepository struct {
	db *database.DB
}

func NewDBRepository(db *database.DB) *DBRepository {
	return &DBRepository{db}
}

func (r *DBRepository) SetCurrency(ctx context.Context, id expense.UserId, currency money.Currency) error {
	const query = `
		insert into users (id, currency_code)
		values ($1, $2)
		on conflict (id) do update
		set currency_code = EXCLUDED.currency_code
	`

	_, err := r.db.ExecContext(ctx, query, id, currency.Code)
	if err != nil {
		return errors.Wrap(err, "error while set user currency")
	}

	return nil
}

func (r *DBRepository) GetCurrency(ctx context.Context, id expense.UserId) (money.Currency, error) {

	const getQuery = "select currency_code from users where id = $1"
	var currencyCode string
	err := r.db.QueryRowContext(ctx, getQuery, id).Scan(&currencyCode)

	if err == sql.ErrNoRows {
		return money.RUB, nil
	}

	if currencyCode == "" {
		return money.RUB, nil
	}

	currency, err := money.CurrencyByCode(currencyCode)
	if err != nil {
		return money.RUB, errors.Wrap(err, "error while get user currency")
	}
	return currency, nil
}

func (r *DBRepository) SetLimit(ctx context.Context, id expense.UserId, limit money.Money) error {
	if limit.Currency != money.RUB {
		return ErrLimitsOnlyInRubble
	}

	const query = `
		insert into users (id, month_limit)
		values ($1, $2)
		on conflict (id) do update
		set month_limit = EXCLUDED.month_limit
	`

	_, err := r.db.ExecContext(ctx, query, id, limit.Value)
	if err != nil {
		return errors.Wrap(err, "error while set user limit")
	}

	return nil
}

func (r *DBRepository) GetLimit(ctx context.Context, id expense.UserId) (money.Money, error) {
	const getQuery = "select month_limit from users where id = $1"

	var value sql.NullInt64
	var limit int
	err := r.db.QueryRowContext(ctx, getQuery, id).Scan(&value)
	if err == sql.ErrNoRows {
		return money.NewRub(0, 0), nil
	}

	if err != nil {
		return money.NewRub(0, 0), errors.Wrap(err, "error while get user limit")
	}

	if value.Valid {
		limit = int(value.Int64)
	} else {
		limit = 0
	}

	return money.Money{Value: limit, Currency: money.RUB}, nil
}
