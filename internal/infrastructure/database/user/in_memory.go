package expenseDatabase

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

var (
	ErrLimitsOnlyInRubble = errors.New("Limit have to be in Rubble")
)

type UserCurrencyStorage interface {
	SetCurrency(context.Context, expense.UserId, money.Currency) error
	GetCurrency(context.Context, expense.UserId) (money.Currency, error)
	SetLimit(context.Context, expense.UserId, money.Money) error
	GetLimit(context.Context, expense.UserId) (money.Money, error)
}

type InMemoryRepository struct {
	currency map[expense.UserId]money.Currency
	limits   map[expense.UserId]money.Money
}

func NewInMemoryRepository() *InMemoryRepository {
	return &InMemoryRepository{
		currency: make(map[expense.UserId]money.Currency),
		limits:   make(map[expense.UserId]money.Money),
	}
}

func (r *InMemoryRepository) SetCurrency(ctx context.Context, id expense.UserId, currency money.Currency) error {
	r.currency[id] = currency
	return nil
}

func (r *InMemoryRepository) GetCurrency(ctx context.Context, id expense.UserId) (money.Currency, error) {
	currency, ok := r.currency[id]
	if !ok {
		return money.RUB, nil
	}

	return currency, nil
}

func (r *InMemoryRepository) SetLimit(ctx context.Context, id expense.UserId, limit money.Money) error {
	if limit.Currency != money.RUB {
		return ErrLimitsOnlyInRubble
	}

	r.limits[id] = limit
	return nil
}

func (r *InMemoryRepository) GetLimit(ctx context.Context, id expense.UserId) (money.Money, error) {
	limit, ok := r.limits[id]
	if !ok {
		return money.NewRub(0, 0), nil
	}

	return limit, nil
}
