package expenseDatabase

import (
	"context"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_ThreadSafeTest(t *testing.T) {
	repo := NewThreadSafeUserDataStorage(NewInMemoryRepository())
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		for i := 0; i < 100; i++ {
			err := repo.SetCurrency(context.Background(), expense.UserId(1), money.USD)
			assert.NoError(t, err)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < 100; i++ {
			_, err := repo.GetCurrency(context.Background(), expense.UserId(1))
			assert.NoError(t, err)
		}
		wg.Done()
	}()

	wg.Wait()
}
