package expenseDatabase

import (
	"testing"

	_ "github.com/lib/pq"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
)

func Test_DB_GetRubbleByDefault(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestGetRubbleByDefault(t, repo)
}

func Test_DB_GetRubbleByDefaultEvenUserHaveLimit(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestGetRubbleByDefaultEvenUserHaveLimit(t, repo)
}

func Test_DB_GetPreviousSetCurrency(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestGetPreviousSetCurrency(t, repo)
}

func Test_DB_GetLimitForNotExistsUser(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestGetLimitForNotExistsUser(t, repo)
}

func Test_DB_SetLimit(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSetLimit(t, repo)
}

func Test_DB_ErrorIfSetLimitNotInRubble(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestErrorIfSetLimitNotInRubble(t, repo)
}

func Test_DB_GetLimitAfterUserSetCurrency(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestGetLimitAfterUserSetCurrency(t, repo)
}
