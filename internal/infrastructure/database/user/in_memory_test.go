package expenseDatabase

import (
	"testing"
)

func Test_GetRubbleByDefault(t *testing.T) {
	repo := NewInMemoryRepository()
	TestGetRubbleByDefault(t, repo)
}

func Test_GetRubbleByDefaultEvenUserHaveLimit(t *testing.T) {
	repo := NewInMemoryRepository()
	TestGetRubbleByDefaultEvenUserHaveLimit(t, repo)
}

func Test_GetPreviousSetCurrency(t *testing.T) {
	repo := NewInMemoryRepository()
	TestGetPreviousSetCurrency(t, repo)
}

func Test_GetLimitForNotExistsUser(t *testing.T) {
	repo := NewInMemoryRepository()
	TestGetLimitForNotExistsUser(t, repo)
}

func Test_SetLimit(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSetLimit(t, repo)
}

func Test_ErrorIfSetLimitNotInRubble(t *testing.T) {
	repo := NewInMemoryRepository()
	TestErrorIfSetLimitNotInRubble(t, repo)
}

func Test_GetLimitAfterUserSetCurrency(t *testing.T) {
	repo := NewInMemoryRepository()
	TestGetLimitAfterUserSetCurrency(t, repo)
}
