package expenseDatabase

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func TestGetRubbleByDefault(t *testing.T, repo UserCurrencyStorage) {
	currency, err := repo.GetCurrency(context.Background(), expense.UserId(1))
	assert.NoError(t, err)
	assert.Equal(t, money.RUB, currency)
}

func TestGetPreviousSetCurrency(t *testing.T, repo UserCurrencyStorage) {
	err := repo.SetCurrency(context.Background(), expense.UserId(1), money.USD)
	assert.NoError(t, err)

	currency, err := repo.GetCurrency(context.Background(), expense.UserId(1))
	assert.NoError(t, err)
	assert.Equal(t, money.USD, currency)
}

func TestGetLimitForNotExistsUser(t *testing.T, repo UserCurrencyStorage) {
	limit, err := repo.GetLimit(context.Background(), expense.UserId(1))
	assert.NoError(t, err)
	assert.Equal(t, money.RUB, limit.Currency)
	assert.Equal(t, 0, limit.Value)
}

func TestSetLimit(t *testing.T, repo UserCurrencyStorage) {
	err := repo.SetLimit(
		context.Background(),
		expense.UserId(1),
		money.NewRub(100, 10),
	)
	assert.NoError(t, err)

	limit, err := repo.GetLimit(context.Background(), expense.UserId(1))
	assert.NoError(t, err)
	assert.Equal(t, money.RUB, limit.Currency)
	assert.Equal(t, 10010, limit.Value)
}

func TestErrorIfSetLimitNotInRubble(t *testing.T, repo UserCurrencyStorage) {
	err := repo.SetLimit(
		context.Background(),
		expense.UserId(1),
		money.NewUsd(100, 10),
	)
	assert.ErrorIs(t, err, ErrLimitsOnlyInRubble)
}

func TestGetRubbleByDefaultEvenUserHaveLimit(t *testing.T, repo UserCurrencyStorage) {
	err := repo.SetLimit(
		context.Background(),
		expense.UserId(1),
		money.NewRub(100, 10),
	)
	assert.NoError(t, err)

	currency, err := repo.GetCurrency(context.Background(), expense.UserId(1))
	assert.NoError(t, err)
	assert.Equal(t, money.RUB, currency)
}

func TestGetLimitAfterUserSetCurrency(t *testing.T, repo UserCurrencyStorage) {
	err := repo.SetCurrency(context.Background(), expense.UserId(1), money.USD)
	assert.NoError(t, err)

	limit, err := repo.GetLimit(context.Background(), expense.UserId(1))
	assert.NoError(t, err)
	assert.Equal(t, money.RUB, limit.Currency)
	assert.Equal(t, 0, limit.Value)
}
