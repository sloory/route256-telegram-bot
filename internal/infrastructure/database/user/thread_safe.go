package expenseDatabase

import (
	"context"
	"sync"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/expense"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type ThreadSafeUserDataStorage struct {
	inner UserCurrencyStorage
	mutex sync.RWMutex
}

func NewThreadSafeUserDataStorage(inner UserCurrencyStorage) *ThreadSafeUserDataStorage {
	return &ThreadSafeUserDataStorage{inner: inner, mutex: sync.RWMutex{}}
}

func (r *ThreadSafeUserDataStorage) SetCurrency(ctx context.Context, id expense.UserId, currency money.Currency) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.inner.SetCurrency(ctx, id, currency)
}

func (r *ThreadSafeUserDataStorage) GetCurrency(ctx context.Context, id expense.UserId) (money.Currency, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	return r.inner.GetCurrency(ctx, id)
}

func (r *ThreadSafeUserDataStorage) SetLimit(ctx context.Context, id expense.UserId, money money.Money) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.inner.SetLimit(ctx, id, money)
}

func (r *ThreadSafeUserDataStorage) GetLimit(ctx context.Context, id expense.UserId) (money.Money, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	return r.inner.GetLimit(ctx, id)
}
