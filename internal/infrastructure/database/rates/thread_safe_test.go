package ratesDatabase

import (
	"context"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func Test_SafeSetAndGetRate(t *testing.T) {
	repo := NewThreadSafeRatesStorage(NewInMemoryRepository())
	err := repo.Set(context.Background(), money.USD, 1.511)
	assert.NoError(t, err)

	err = repo.Set(context.Background(), money.USD, 1.611)
	assert.NoError(t, err)

	rate, err := repo.Get(context.Background(), money.USD)

	assert.NoError(t, err)
	assert.Equal(t, 1.611, rate)
}

func Test_ThreadSafeTest(t *testing.T) {
	repo := NewThreadSafeRatesStorage(NewInMemoryRepository())
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		for i := 0; i < 100; i++ {
			_ = repo.Set(context.Background(), money.USD, 1.511)
		}
		wg.Done()
	}()
	go func() {
		for i := 0; i < 100; i++ {
			_, _ = repo.Get(context.Background(), money.USD)
		}
		wg.Done()
	}()

	wg.Wait()
}
