package ratesDatabase

import (
	"testing"
)

func Test_ErrorForNotKnowRate(t *testing.T) {
	repo := NewInMemoryRepository()
	TestErrorForNotKnowRate(t, repo)
}

func Test_SetAndGetRate(t *testing.T) {
	repo := NewInMemoryRepository()
	TestSetAndGetRate(t, repo)
}
