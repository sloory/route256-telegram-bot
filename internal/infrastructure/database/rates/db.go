package ratesDatabase

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/infrastructure/database"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type DBRepository struct {
	db *database.DB
}

func NewDBRepository(db *database.DB) *DBRepository {
	return &DBRepository{db}
}

func (r *DBRepository) Set(ctx context.Context, currency money.Currency, rate float64) error {
	const query = `
		insert into rates (code, rate)
		values ($1, $2)
		on conflict on constraint rates_unique_code_cst do update
		set rate = EXCLUDED.rate
	`

	_, err := r.db.ExecContext(ctx, query, currency.Code, rate)
	if err != nil {
		return errors.Wrap(err, "error while set rates")
	}

	return nil
}

func (r *DBRepository) Get(ctx context.Context, currency money.Currency) (float64, error) {

	const getQuery = "select rate from rates where code = $1"
	var rate float64
	err := r.db.QueryRowContext(ctx, getQuery, currency.Code).Scan(&rate)
	if err == nil {
		return rate, nil
	}

	if err == sql.ErrNoRows {
		return 0, ErrNotHaveRate
	}

	return 0, errors.Wrap(err, "error while get rates")
}
