package ratesDatabase

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

func TestErrorForNotKnowRate(t *testing.T, repo RatesStorage) {
	_, err := repo.Get(context.Background(), money.USD)

	assert.ErrorIs(t, err, ErrNotHaveRate)
}

func TestSetAndGetRate(t *testing.T, repo RatesStorage) {
	err := repo.Set(context.Background(), money.USD, 1.511)
	assert.NoError(t, err)
	err = repo.Set(context.Background(), money.USD, 1.611)
	assert.NoError(t, err)

	rate, err := repo.Get(context.Background(), money.USD)

	assert.NoError(t, err)
	assert.Equal(t, 1.611, rate)
}
