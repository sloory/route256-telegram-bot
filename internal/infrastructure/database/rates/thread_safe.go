package ratesDatabase

import (
	"context"
	"sync"

	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type ThreadSafeRatesStorage struct {
	inner RatesStorage
	mutex sync.RWMutex
}

func NewThreadSafeRatesStorage(inner RatesStorage) *ThreadSafeRatesStorage {
	return &ThreadSafeRatesStorage{inner: inner, mutex: sync.RWMutex{}}
}

func (r *ThreadSafeRatesStorage) Set(ctx context.Context, currency money.Currency, rate float64) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.inner.Set(ctx, currency, rate)
}

func (r *ThreadSafeRatesStorage) Get(ctx context.Context, currency money.Currency) (float64, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	return r.inner.Get(ctx, currency)
}
