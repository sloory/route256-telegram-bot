package ratesDatabase

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/model/money"
)

type RatesStorage interface {
	Set(context.Context, money.Currency, float64) error
	Get(context.Context, money.Currency) (float64, error)
}

type InMemoryRepository struct {
	data map[string]float64
}

func NewInMemoryRepository() *InMemoryRepository {
	return &InMemoryRepository{
		data: make(map[string]float64),
	}
}

var ErrNotHaveRate = errors.New("don`t have exchange rate")

func (r *InMemoryRepository) Set(ctx context.Context, currency money.Currency, rate float64) error {
	r.data[currency.Code] = rate
	return nil
}

func (r *InMemoryRepository) Get(ctx context.Context, currency money.Currency) (float64, error) {
	rate, ok := r.data[currency.Code]
	if !ok {
		return 0, ErrNotHaveRate
	}

	return rate, nil
}
