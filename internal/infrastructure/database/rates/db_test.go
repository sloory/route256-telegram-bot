package ratesDatabase

import (
	"testing"

	_ "github.com/lib/pq"
	"gitlab.ozon.dev/sloory.d/telegram-bot/internal/tests"
)

func Test_DB_ErrorForNotKnowRate(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestErrorForNotKnowRate(t, repo)
}

func Test_DB_SetAndGetRate(t *testing.T) {
	defer tests.ClearDB()

	repo := NewDBRepository(tests.GetTestDB())
	TestSetAndGetRate(t, repo)
}
