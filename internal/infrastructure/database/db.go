package database

import (
	"context"
	"database/sql"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type DBQuery interface {
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
}
type DB struct {
	conn *sql.DB
	log  *zap.Logger
}

func New(db *sql.DB, log *zap.Logger) *DB {
	return &DB{db, log}
}

var ErrCommitFailed = "Error while commit transaction"

func (db *DB) WithinTransaction(ctx context.Context, tFunc func(ctx context.Context) error) error {
	tx, err := db.conn.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	defer func() {
		if p := recover(); p != nil {
			_ = tx.Rollback()
			panic(p)
		}
	}()

	err = tFunc(injectTx(ctx, tx))

	if err != nil {
		txErr := tx.Rollback()
		if txErr != nil {
			db.log.Error("tx rollback error", zap.Error(txErr))
		}
		return err
	}

	txErr := tx.Commit()
	if txErr != nil {
		return errors.Wrap(txErr, "Error while commit transaction")
	}

	return nil
}

func (db *DB) QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "db query")
	defer span.Finish()

	span.LogKV("query", query)
	return db.getQuery(ctx).QueryContext(ctx, query, args...)
}

func (db *DB) ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "db exec")
	defer span.Finish()

	span.LogKV("query", query)
	return db.getQuery(ctx).ExecContext(ctx, query, args...)
}

func (db *DB) QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row {
	span, ctx := opentracing.StartSpanFromContext(ctx, "db query row")
	defer span.Finish()

	span.LogKV("query", query)
	return db.getQuery(ctx).QueryRowContext(ctx, query, args...)
}

func (db *DB) getQuery(ctx context.Context) DBQuery {
	tx := extractTx(ctx)
	if tx != nil {
		return tx
	} else {
		return db.conn
	}
}

type txKey struct{}

func injectTx(ctx context.Context, tx *sql.Tx) context.Context {
	return context.WithValue(ctx, txKey{}, tx)
}

func extractTx(ctx context.Context) *sql.Tx {
	if tx, ok := ctx.Value(txKey{}).(*sql.Tx); ok {
		return tx
	}
	return nil
}
