package services

import (
	"context"
	"encoding/json"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"io"
	"net/http"

	"github.com/pkg/errors"
)

const ServiceUrl = "https://www.cbr-xml-daily.ru/latest.js"

var (
	errBaseRateNotRub = errors.New("base currency not rubble")
	errServiceNotWork = errors.New("rates service not work")
)

type RatesService struct {
	Url string
	log *zap.Logger
}

func NewRatesService(log *zap.Logger) *RatesService {
	return &RatesService{ServiceUrl, log}
}

func (r *RatesService) Get(ctx context.Context) (map[string]float64, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "get rates")

	result := make(map[string]float64)
	req, err := http.NewRequest("GET", r.Url, nil)
	if err != nil {
		return result, err
	}
	req = req.WithContext(ctx)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return result, errServiceNotWork
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return result, errors.Wrap(err, "while read http body")
	}

	span.Finish()
	span, _ = opentracing.StartSpanFromContext(ctx, "parse rates")

	rates, err := parseResponse(string(bodyBytes))
	if err != nil {
		return result, err
	}

	for currency, rate := range rates {
		if rate <= 0 {
			r.log.Warn(
				"Rate below 0",
				zap.Float64("rate", rate),
				zap.String("currency", currency),
			)
			continue
		}
		result[currency] = 1 / rate
	}
	span.Finish()
	return result, nil
}

func parseResponse(responseStr string) (map[string]float64, error) {
	var response ServiceResponse
	err := json.Unmarshal([]byte(responseStr), &response)
	if err != nil {
		return make(map[string]float64), err
	}

	if response.Base != "RUB" {
		return make(map[string]float64), errBaseRateNotRub
	}

	return response.Rates, nil
}

type ServiceResponse struct {
	Rates map[string]float64
	Base  string
}
