package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ParseResponse(t *testing.T) {
	rates, err := parseResponse(`{
    "disclaimer": "https://www.cbr-xml-daily.ru/#terms",
    "date": "2022-10-08",
    "timestamp": 1665176400,
    "base": "RUB",
    "rates": {
        "USD": 0.016327197,
        "EUR": 0.016673447,
        "KRW": 23.06055,
        "JPY": 2.36777178
    }
}`)

	assert.NoError(t, err)
	assert.Equal(
		t,
		map[string]float64{
			"USD": 0.016327197,
			"EUR": 0.016673447,
			"KRW": 23.06055,
			"JPY": 2.36777178,
		},
		rates,
	)
}

func Test_ParseResponseErrorIfBaseNotRub(t *testing.T) {
	_, err := parseResponse(`{
    "disclaimer": "https://www.cbr-xml-daily.ru/#terms",
    "date": "2022-10-08",
    "timestamp": 1665176400,
    "base": "USD",
    "rates": {
        "USD": 0.016327197,
        "EUR": 0.016673447,
        "KRW": 23.06055,
        "JPY": 2.36777178
    }
}`)

	assert.ErrorIs(t, err, errBaseRateNotRub)

}
