package cache

type NoCache struct {
}

func (c *NoCache) Set(key string, value any) {

}

func (c *NoCache) Get(key string) any {
	return nil
}

func (c *NoCache) Remove(key string) bool {
	return false
}

func (c *NoCache) Len() int {
	return 0
}
