package cache

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestLRU_Len_emptyCache_returnZero(t *testing.T) {
	cache := NewLRUCache(3)

	cacheLen := cache.Len()

	assert.Equal(t, 0, cacheLen)
}

func TestLRU_Set_existKeyToFullCache_itLastUsed(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("someKey2", "56")
	cache.Set("someKey3", 11)

	cache.Set("someKey1", 10)

	assert.Equal(t, "someKey1", getLastUsedKey(cache))
	assert.Equal(t, "someKey2", getLeastUsedKey(cache))
	assert.Equal(t, 3, cache.Len())
}

func TestLRU_Set_existKeyToNotFull_itLastUsed(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("someKey2", "56")

	cache.Set("someKey1", 10)

	assert.Equal(t, "someKey1", getLastUsedKey(cache))
	assert.Equal(t, "someKey2", getLeastUsedKey(cache))
	assert.Equal(t, 2, cache.Len())
}

func TestLRU_Set_newKeyToFullCache_leastUsedKeyRemoved(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("someKey2", "56")
	cache.Set("someKey3", 9)

	cache.Set("new key", 5)

	assert.Nil(t, cache.Get("someKey1"))
	assert.Equal(t, "new key", getLastUsedKey(cache))
	assert.Equal(t, "someKey2", getLeastUsedKey(cache))
	assert.Equal(t, 3, cache.Len())
}

func TestLRU_Set_newKeyAsync_allKeysExists(t *testing.T) {
	//Arrange
	wg := sync.WaitGroup{}
	cache := NewLRUCache(3)
	wg.Add(3)

	//Act
	go func() {
		cache.Set("someKey1", 8)
		wg.Done()
	}()
	go func() {
		cache.Set("someKey2", "56")
		wg.Done()
	}()
	go func() {
		cache.Set("someKey3", 7)
		wg.Done()
	}()

	wg.Wait()

	//Assert
	assert.Equal(t, 8, cache.Get("someKey1"))
	assert.Equal(t, "56", cache.Get("someKey2"))
	assert.Equal(t, 7, cache.Get("someKey3"))
	assert.Equal(t, 3, cache.Len())
}

func TestLRU_Get_existsKey_returnItAndMoveToFront(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("someKey2", 5)
	cache.Set("someKey3", "90")

	item := cache.Get("someKey2")

	assert.Equal(t, 5, item)
	assert.Equal(t, "someKey2", getLastUsedKey(cache))
	assert.Equal(t, "someKey1", getLeastUsedKey(cache))
	assert.Equal(t, 3, cache.Len())
}

func TestLRU_Get_notExistsKey_returnNil(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("someKey2", 5)
	cache.Set("someKey3", "90")

	item := cache.Get("not exist key")

	assert.Nil(t, item)
	assert.Equal(t, "someKey3", getLastUsedKey(cache))
	assert.Equal(t, "someKey1", getLeastUsedKey(cache))
	assert.Equal(t, 3, cache.Len())
}

func TestLRU_Remove_existsElement_removeIt(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("existsKey", 5)
	cache.Set("someKey3", "90")

	result := cache.Remove("existsKey")

	assert.Nil(t, cache.Get("existsKey"))
	assert.True(t, result)
	assert.Equal(t, 2, cache.Len())
	assert.Equal(t, 8, cache.Get("someKey1"))
	assert.Equal(t, "90", cache.Get("someKey3"))
}

func TestLRU_Remove_notExistsKey_returnFalse(t *testing.T) {
	cache := NewLRUCache(3)
	cache.Set("someKey1", 8)
	cache.Set("someKey2", 5)

	result := cache.Remove("not exists key")

	assert.False(t, result)
	assert.Equal(t, 2, cache.Len())
	assert.Equal(t, 8, cache.Get("someKey1"))
	assert.Equal(t, 5, cache.Get("someKey2"))
}

func getLastUsedKey(cache *LRUCache) string {
	return cache.syncCache.queue.Front().Value.(*cacheItem).Key
}

func getLeastUsedKey(cache *LRUCache) string {
	return cache.syncCache.queue.Back().Value.(*cacheItem).Key
}
