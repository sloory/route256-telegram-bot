package cache

import (
	"container/list"
	"sync"
)

type cacheItem struct {
	Key   string
	Value any
}

type LRUCache struct {
	mutex     *sync.RWMutex
	syncCache *SyncLRUCache
}

type SyncLRUCache struct {
	capacity int
	queue    *list.List
	items    map[string]*list.Element
}

func NewSyncLRUCache(capacity int) *SyncLRUCache {
	return &SyncLRUCache{
		capacity: capacity,
		queue:    list.New(),
		items:    make(map[string]*list.Element),
	}
}

func NewLRUCache(capacity int) *LRUCache {
	return &LRUCache{
		syncCache: NewSyncLRUCache(capacity),
		mutex:     &sync.RWMutex{},
	}
}

func (c *LRUCache) Set(key string, value any) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.syncCache.Set(key, value)
}

func (c *LRUCache) Get(key string) any {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	return c.syncCache.Get(key)
}

func (c *LRUCache) Remove(key string) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	return c.syncCache.Remove(key)
}

func (c *LRUCache) Len() int {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	return c.syncCache.Len()
}

func (c *SyncLRUCache) Set(key string, value any) {
	if item, exists := c.items[key]; exists {
		c.queue.MoveToFront(item)
		item.Value.(*cacheItem).Value = value
		return
	}

	if c.isFull() {
		c.removeLeastUsed()
	}

	item := &cacheItem{
		Key:   key,
		Value: value,
	}

	element := c.queue.PushFront(item)
	c.items[item.Key] = element
}

func (c *SyncLRUCache) Get(key string) any {
	item, exists := c.items[key]
	if !exists {
		return nil
	}

	c.queue.MoveToFront(item)
	return item.Value.(*cacheItem).Value
}

func (c *SyncLRUCache) Remove(key string) bool {
	if item, found := c.items[key]; found {
		c.deleteItem(item)
		return true
	}

	return false
}

func (c *SyncLRUCache) Len() int {
	return len(c.items)
}

func (c *SyncLRUCache) removeLeastUsed() {
	if element := c.queue.Back(); element != nil {
		c.deleteItem(element)
	}
}

func (c *SyncLRUCache) deleteItem(element *list.Element) {
	item := c.queue.Remove(element).(*cacheItem)
	delete(c.items, item.Key)
}

func (c *SyncLRUCache) isFull() bool {
	return c.Len() == c.capacity
}
